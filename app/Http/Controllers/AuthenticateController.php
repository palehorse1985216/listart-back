<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk;
use Facebook\Exceptions\FacebookSDKException;
use Google_Client;
use Google_Service_People;
use Google_Service_PeopleService;
use App\Http\Models\User;
use App\Http\Models\Validation;
use Twitter;
use Session;
use Curl;
use Storage;

class AuthenticateController extends Controller
{
	protected static $_platform = [
		1 => "Facebook",
		2 => "Google",
		3 => "Twitter",
		4 => "Email",
	];

	protected static $_error = [
		"101" => "權杖遺失或錯誤",
		"102" => "無法取得個人資料",
		"103" => "帳號被停用",
		"104" => "無法設定權杖",
		"105" => "權杖更新失敗",
		"106" => "驗證碼錯誤或已逾期",
	];

    public function facebook(LaravelFacebookSdk $LaravelFacebookSdk) {
    	// 登入型態為Facebook
    	$type = 1;
    	try {
	        $token = $LaravelFacebookSdk->getAccessTokenFromRedirect(env('APP_URL') . '/authenticate/facebook');
	        if (empty($token)) {
	        	throw new FacebookSDKException(self::$_error[101], 101);
	        }
	    } catch (FacebookSDKException $e) {		    
	    	return redirect()->route('authenticate-failure', [$type, $e->getCode()]);
	    }

	    if (! $token->isLongLived()) {
            $oauth_client = $fb->getOAuth2Client();

            // 取得長期權杖(token)
			try {
	            $token = $oauth_client->getLongLivedAccessToken($token);
	        } catch (FacebookSDKException $e) {
	            return redirect()->route('authenticate-failure', [$type, 101]);
	        }
	    }

	    $LaravelFacebookSdk->setDefaultAccessToken($token);

	    // 取得使用者在Facebook的個人資料
	    try {
	    	$fields = [
	    		'id',
	    		'name',
	    		'email',
	    		'picture.width(500)',
	    	];
	        $response = $LaravelFacebookSdk->get('/me?fields=' . implode(',', $fields));
	        if (empty($response)) {
	        	throw new FacebookSDKException(self::$_error[102], 102);
	        }
	    } catch (FacebookSDKException $e) {
	        return redirect()->route('authenticate-failure', [$type, 102]);
    	}

    	$facebookUser = $response->getGraphUser();
    	$facebook_user_id = $facebookUser->getID();
    	$User = User::where('user_external_id', $facebook_user_id)
    				->first();
    	$user_info = [];
    	if (empty($User)) {
    		$User = new User;
    		$User->user_external_id = $user_info['user_external_id']
    		                        = $facebook_user_id;

    		$User->user_login_type  = $type;

    		$User->user_status      = $user_info['user_status']
    		                        = 1;

    		$User->create_time = date('Y-m-d H:i:s');
    	} else {
    		if ($User->user_status == 0) {
    			return redirect()->route('authenticate-failure', [$type, 103]);
    		}
    	}

    	$User->user_external_token = $user_info['user_external_token'] 
    	                           = $LaravelFacebookSdk->getDefaultAccessToken()->getValue();

    	$User->user_name           = $user_info['user_name']
    						       = $facebookUser->getName();

    	$User->user_external_email = $user_info['user_external_email']
    	                           = $facebookUser->getEmail();

    	$User->last_login = date('Y-m-d H:i:s');
    	$User->save();
    	$user_info['user_id'] = $User->user_id;
        $user_info['user_login_type'] = $User->user_login_type;

    	if (empty($User->user_avatar) || !preg_match('/upload\//', $User->user_avatar)) {
    		$picture_url = $facebookUser->getPicture()->getUrl();
    		$picture_response = Curl::to($picture_url)
    								->returnResponseObject()
		            			    ->get();
		    if ($picture_response->status == 200) {
		    	if (preg_match('/^image\/(\w+)$/', $picture_response->contentType, $matches)) {
		    		$ext = isset($matches[1]) ? $matches[1] : "";
		    		$time = time();
		    		Storage::disk('public')->put('avatars/facebook/'.$User->user_id.'-'.$time.'.'.$ext, $picture_response->content);
		    		$User->user_avatar = 'avatars/facebook/'.$User->user_id.'-'.$time.'.'.$ext;
		    		$User->save();
		    		$user_info['user_avatar'] = Storage::url($User->user_avatar);
		    	}
		    }
    	} else {
    		$user_info['user_avatar'] = !empty($User->user_avatar) ? Storage::disk('public')->url($User->user_avatar) : '';
    	}

    	Session::put('user', $user_info);
    	return redirect()->route('item-show');
    }

    public function google(Request $request) {
    	$type = 2;
    	$code = $request->input('code');
    	if (empty($code)) {
    		return redirect()->route('authenticate-failure', [$type, 101]);
    	}

    	$GoogleClient = new Google_Client;
        $GoogleClient->setClientId(env('GOOGLE_CLIENT_ID'));
        $GoogleClient->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $GoogleClient->addScope(Google_Service_People::USERINFO_PROFILE);
        $GoogleClient->setRedirectUri(env('APP_URL') . '/authenticate/google');
        //取得權杖
        $token = $GoogleClient->fetchAccessTokenWithAuthCode($code);
        try {
        	//設定取得的權杖
        	$GoogleClient->setAccessToken($token);
    	} catch (InvalidArgumentException $e) {
    		return redirect()->route('authenticate-failure', [$type, 104]);
    	}

    	//假如權杖過期，向Google更新權杖
    	if ($GoogleClient->isAccessTokenExpired()) {
	    	try {
		        $GoogleClient->fetchAccessTokenWithRefreshToken($GoogleClient->getRefreshToken());
		    } catch (LogicException $e) {
		    	return redirect()->route('authenticate-failure', [$type, 105]);
			}
			//重新取得權杖
			$token = $GoogleClient->getAccessToken();
	    }

	    $PeopleService = new Google_Service_PeopleService($GoogleClient);
	    $fields = ['names', 'emailAddresses', 'photos'];
	    $googleUser = $PeopleService->people->get('people/me', ['personFields' => implode(',', $fields)]);
	    $google_user_id = $GoogleClient->verifyIdToken($token['id_token'])['sub'];
	    $User = User::where('user_external_id', $google_user_id)
	    			->first();
	    $user_info = [];
	    if (empty($User)) {
	    	$User = new User;
	    	$User->user_external_id = $user_info['user_external_id']
	    							= $google_user_id;
	    	$User->user_login_type  = $type;
	    	$User->user_status      = $user_info['user_status']
	    							= 1;
	    	$User->create_time      = date('Y-m-d H:i:s');
	    } else {
	    	if ($User->user_status == 0) {
    			return redirect()->route('authenticate-failure', [$type, 103]);
    		}
	    }
	    $User->user_external_email   = $user_info['user_external_email']
	    							 = $googleUser->getEmailAddresses()[0]		   ->getValue();
	    $User->user_name             = $user_info['user_name']
	                                 = $googleUser->getNames()[0]
	                                 			  ->getDisplayName();
	    
	    $User->last_login = date('Y-m-d H:i:s');
	    $User->save();
        $user_info['user_id'] = $User->user_id;
        $user_info['user_login_type'] = $User->user_login_type;

	    if (empty($User->user_avatar) || !preg_match('/upload\//', $User->user_avatar)) {
	    	$response = Curl::to($googleUser['photos'][0]['url'])
                    		->withData(array('sz' => 500))
							->returnResponseObject()
		            		->get();
		            	
		    if (preg_match('/^image\/(\w+)$/', $response->contentType, $matches)) {
				$ext = isset($matches[1]) ? $matches[1] : '';
				$time = time();
				Storage::disk('public')->put('avatars/google/'.$User->user_id.'-'.$time.'.'.$ext, $response->content);
				$User->user_avatar = 'avatars/google/'.$User->user_id.'-'.$time.'.'.$ext;
				$User->save();
				$user_info['user_avatar'] = Storage::url($User->user_avatar);
		    }
	    } else {
    		$user_info['user_avatar'] = !empty($User->user_avatar) ? Storage::disk('public')->url($User->user_avatar) : '';
    	}
	    
	    Session::put('user', $user_info);
    	return redirect()->route('item-show');
    }

    public function twitter(Request $request) {
        $type = 3;
        Twitter::reconfig([
                    'token'  => Session::get('oauth_token'),
                    'secret' => Session::get('oauth_secret')
                 ]);

        if (empty($request->oauth_verifier)) {
            return redirect()->route('authenticate-failure', [$type, 101]);
        }

        try {
            $token = Twitter::getAccessToken($request->oauth_verifier);
            if (!isset($token['oauth_token_secret'])) {
                throw new Exception();
            }
        } catch (Exception $e) {
            return redirect()->route('authenticate-failure', [$type, 101]);
        }

        $credentials = Twitter::getCredentials(['include_email' => 'true']);
        if (!is_object($credentials) || isset($credentials->error)) {
            return redirect()->route('authenticate-failure', [$type, 101]);
        }
        
        $User = User::where('user_external_id', $credentials->id)->first();
        if (empty($User)) {
            $User = new User;
            $User->user_external_id = $user_info['user_external_id']
                                    = $credentials->id;
            $User->user_login_type  = $type;
            $User->user_status      = $user_info['user_status']
                                    = 1;
            $User->create_time      = date('Y-m-d H:i:s');
        } else {
            if ($User->user_status == 0) {
                return redirect()->route('authenticate-failure', [$type, 103]);
            }
        }

        $User->user_external_token = $user_info['user_external_token'] 
                                   = Session::get('oauth_token');

        $User->user_name           = $user_info['user_name']
                                   = $credentials->name;

        $User->user_external_email = $user_info['user_external_email']
                                   = $credentials->email;

        $User->last_login = date('Y-m-d H:i:s');
        $User->save();

        $user_info['user_id'] = $User->user_id;
        $user_info['user_login_type'] = $User->user_login_type;
        if (empty($User->user_avatar) || !preg_match('/upload\//', $User->user_avatar)) {
            $response = Curl::to(str_replace('_normal', '', $credentials->profile_image_url_https))
                            ->returnResponseObject()
                            ->get();
                        
            if (preg_match('/^image\/(\w+)$/', $response->contentType, $matches)) {
                $ext = isset($matches[1]) ? $matches[1] : '';
                $time = time();
                Storage::disk('public')->put('avatars/twitter/'.$User->user_id.'-'.$time.'.'.$ext, $response->content);
                $User->user_avatar = 'avatars/twitter/'.$User->user_id.'-'.$time.'.'.$ext;
                $User->save();
                $user_info['user_avatar'] = Storage::url($User->user_avatar);
            }
        } else {
            $user_info['user_avatar'] = !empty($User->user_avatar) ? Storage::disk('public')->url($User->user_avatar) : '';
        }
        
        Session::put('user', $user_info);
        return redirect()->route('item-show');
    }

    public function email($validation_code) {
    	$type = 4;
    	$Validation = Validation::where('validation_code', $validation_code)->where('create_time', '>', date('Y-m-d H:i:s', strtotime('-1 hour')))->first();
    	if (empty($Validation)) {
    		return redirect()->route('authenticate-failure', [$type, 106]);
    	}
    	$user_email = $Validation->user_email;
    	$User = User::where('user_external_email', $user_email)->first();
    	$user_info = [];
    	if (empty($User)) {
    		$User = new User;
    		$User->user_external_email = $user_info['user_external_email']
    		                           = $user_email;
    		$User->user_login_type     = $user_info['user_login_type']
    								   = $type;
    		$User->user_status         = $user_info['user_status']
	    							   = 1;
    		$User->user_name           = $user_info['user_name']
    								   = preg_replace('/@\w+.\w+/', '', $user_email);
    		$User->create_time    	   = $user_info['create_time']
    								   = date('Y-m-d H:i:s');
    	} else {
    		if ($User->user_status != 1) {
    			return redirect()->route('authenticate-failure', [$type, 103]);
    		}
    	}
    	$User->last_login = date('Y-m-d H:i:s');
    	$User->save();
        $user_info['user_id'] = $User->user_id;
    	Session::put('user', $user_info);

    	if (empty($User->user_password)) {
    		return redirect()->route('user-password');
    	}
    	return redirect()->route('item-show');
    }

    public function failure($type, $code) {
    	$data = [];
    	$data['title'] = self::$_platform[$type] . "登入逾期";
    	$data['message'] = self::$_error[$code];
    	return view('authenticate/failure', $data);
    }
}
