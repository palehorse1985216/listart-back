<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use Session;
use Storage;

class DownloadController extends Controller
{
    public function avatar() {
    	$user_id = Session::get('user')['user_id'];
    	$User = User::find($user_id);
    	if (!empty($User)) {
    		return Storage::disk('public')->download($User->user_avatar);
    	}
    	return false;
    }
}
