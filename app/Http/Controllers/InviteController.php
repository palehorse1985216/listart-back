<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Project;
use App\Http\Models\Member;
use Session;

class InviteController extends Controller
{
	public static $_error = [
								"101" => "您並未受邀",
								"102" => "您已是專案的成員",
							];
    public function acceptInvitation($user_id, $project_id) {
    	$Member = Member::where('user_id', $user_id)
    					->where('project_id', $project_id)
    					->first();
    	if (empty($Member)) {
    		return redirect()->route('project-failure', [101]);
    	}

    	if ($Member->member_status == 1) {
    		return redirect()->route('project-failure', [102]);
    	}
    	
    	$Member->member_status = 1;
    	$Member->save();
    	return redirect()->route('project-show');
    }

    public function rejectInvitation($user_id, $project_id) {
    	$Member = Member::where('user_id', $user_id)
    					->where('project_id', $project_id)
    					->first();
    	if (empty($Member)) {
    		return redirect()->route('project-failure', [101]);
    	}

    	if ($Member->member_status == 1) {
    		return redirect()->route('project-failure', [102]);
    	}

    	$Member->delete();
    	return redirect()->route('project-show');
    }

    public function failure($code) {
    	$data = [];
    	$data['title'] = "錯誤";
    	$data['message'] = isset(self::$_error[$code]) ? self::$_error[$code] : "資料錯誤
    	";
    	return view('project.failure', $data);
    }
}
