<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class ItemController extends Controller
{
    public function show() {
    	return view('item.content');
    }
}
