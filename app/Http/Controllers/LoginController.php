<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;

class LoginController extends Controller
{
    public function show() {
    	$data = [];
    	$data['title'] = "登入";
    	$data['js'] = 'login-' . __FUNCTION__;
        $data['css'] = 'login-' . __FUNCTION__;
    	return view('login.content', $data);
    }

    public function password() {
    	$data = [];
        $data['title'] = "密碼登入";
        $data['js'] = 'login-' . __FUNCTION__;
        $data['css'] = 'login-' . __FUNCTION__;
        return view('login.password', $data);
    }
}
