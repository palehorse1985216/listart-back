<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PrivacyController extends Controller
{
    public function policy() {
    	$data = [];
    	$data['css'] = 'privacy-policy';
    	$data['js'] = 'privacy-policy';
    	return view('privacy.policy', $data);
    }
}
