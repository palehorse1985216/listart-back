<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Project;
use App\Http\Models\Member;
use Session;

class ProjectController extends Controller
{
	protected static $_error = [
		"101" => "您並非專案的成員",
		"102" => "您並非專案的管理者",
	];

    public function show(Request $request) {
    	if (empty($request->project)) {
    		$user_id = Session::get('user')['user_id'];
	    	$Member = Member::where('user_id', $user_id)->first();
	    	if (!empty($Member)) {
	    		return redirect('/project/show?project=' . $Member->project_id);
	    	}
	    }
    	return view('project.content');
    }

    public function manage(Request $request) {
        $data = [];
        $data['css'] = $data['js'] = 'project-manage';
        $project_id = $data['project_id'] = $request->project;
        $Project = Project::find($project_id);
        $data['name'] = $Project->project_name;
        $Members = Member::where('project_id', $project_id)->get();
        foreach ($Members as $key => $Member) {
            $User = User::find($Member->user_id);
            if (empty($User)) {
                $Members->splice($key, 1);
                continue;
            }
            $Member->name = !empty($User->user_name) ? $User->user_name : (!empty($User->user_nickname) ? $User->user_nickname : '');
            $Member->email = $User->user_external_email;
            switch ($Member->project_role) {
                case 'admin':
                    $Member->role = "專案管理者";
                    break;
                case 'member':
                    $Member->role = "一般成員";
                    break;
                default:
                    $Member->role = "";
            }
        }
        $data['role'] = Member::where('project_id', $project_id)
                              ->where('user_id', Session('user')['user_id'])
                              ->first()->project_role;
        $data['Members'] = $Members;
        return view('project.project-manage', $data);
    }

    public function failure($code) {
    	$data = [];
    	$data['title'] = "錯誤";
    	$data['message'] = isset(self::$_error[$code]) ? self::$_error[$code] : "資料錯誤
    	";
    	return view('project.failure', $data);
    }
}
