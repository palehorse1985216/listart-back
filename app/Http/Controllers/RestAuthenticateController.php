<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use Session;

class RestAuthenticateController extends Controller
{
    public function password(Request $request) {
    	$user = Session::get('user');
    	$User = User::find($user['user_id']);
    	$user['last_login'] = $User->last_login = date('Y-m-d H:i:s');
    	$User->save();

    	Session::put('user', $user);
    	$ret = [];
    	$ret['status'] = "OK";
    	$ret['message'] = "登入成功";
    	return response()->json($ret);
    }
}
