<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Category;
use App\Http\Models\Item;
use App\Http\Models\Relationship;
use Session;
use DB;

class RestCategoryController extends Controller
{
	public function getCategory() {
		$user_id = Session::get('user')['user_id'];
		$Category = Category::where('user_id', $user_id)
                            ->orderBy('category_id', 'asc')
                            ->get();

		$ret = [];
		$ret['status'] = "OK";
		$ret['data'] = $Category->toArray();
		$ret['count'] = $Category->count();
		return response()->json($ret);
	}

	public function fetchCategory($category_id) {
		$ret = [];
		$Category = Category::find($category_id);
		if (empty($Category)) {
			$ret['status'] = "Not Found";
			$ret['message'] = "無此分類";
            return response()->json($ret, 404);
		}

		$ret['status'] = "OK";
        $ret['data'] = $Category->toArray();
        return response()->json($ret);
	}

    public function createCategory(Request $request) {
    	$name = $request->name;
    	$user_id = Session::get('user')['user_id'];
    	$Category = new Category;
    	$Category->category_name = $name;
    	$Category->user_id = $user_id;
    	$Category->create_time = date('Y-m-d H:i:s');
    	$Category->save();

    	$ret = [];
    	$ret['status'] = "OK";
    	$ret['message'] = "建立成功";
        $ret['category_id'] = $Category->category_id;
    	return response()->json($ret);
    }

    public function updateCategory(Request $request) {
    	$category_id = $request->category_id;
    	$name = $request->name;
    	$user_id = Session::get('user')['user_id'];

    	$Category = Category::find($category_id);
    	$Category->category_name = $name;
    	$Category->user_id = $user_id;
    	$Category->update_time = date('Y-m-d H:i:s');
    	$Category->save();

    	$ret = [];
    	$ret['status'] = "OK";
    	$ret['message'] = "修改成功";
    	return response()->json($ret);
    }

    public function deleteCategory(Request $request) {
        $category_id = $request->category_id;
        $user_id = Session::get('user')['user_id'];

        $Category = Category::find($category_id);
        $Category->delete();

        $Relationship = Relationship::where('category_id', $category_id)
                                    ->where('user_id', $user_id)
                                    ->get();
        $to_delete_items = [];
        foreach ($Relationship as $row) {
            $to_delete_items[] = $row->item_id;
            $row->delete();
        }

        if ($request->is_items_deleted) {
            $Item = Item::whereIn('item_id', $to_delete_items)->get();
            foreach ($Item as $row) {
                $row->delete();
            }
        }

        $ret = [];
        $ret['status'] = "OK";
        $ret['message'] = "刪除成功";
        return response()->json($ret);
    }
}
