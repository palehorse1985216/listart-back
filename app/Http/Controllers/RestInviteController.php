<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use App\Http\Models\Project;
use App\Http\Models\Member;
use App\Mail\MemberInvitation;
use Illuminate\Support\Facades\Mail;
use Session;

class RestInviteController extends Controller
{
    public function sendInvitation(Request $request) {
    	$ret = [];
        $email = $request->email;
        $User = User::where('user_external_email', $request->email)
        			->first();
        if (empty($User)) {
        	$ret['status'] = "User Not Found";
        	$ret['message'] = "受邀成員不存在";
        	return response()->json($ret, 404);
        }

        $Project = Project::find($request->project);
        if (empty($Project)) {
        	$ret['status'] = "Project Not Found";
        	$ret['message'] = "專案不存在";
        	return response()->json($ret, 404);
        }

        $Member = new Member;
        $Member->user_id = $User->user_id;
        $Member->project_id = $request->project;
        $Member->project_role = "member";
        $Member->member_status = 0;
        $Member->create_time = date('Y-m-d H:i:s');
        $Member->save();

        Mail::to($request->email)
            ->send(new MemberInvitation(Session('user')['user_id'],
            							$User->user_id, 
            							$Project->project_id));
        $ret['status'] = "OK";
        $ret['message'] = "已發送邀請給 " . $request->email;
        return response()->json($ret);
    }
}
