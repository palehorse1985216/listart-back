<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Item;
use App\Http\Models\Relationship;
use App\Http\Models\Includes;
use Session;
use DB;

class RestItemController extends Controller
{
    public static function getItem($status = NULL, Request $request) {
    	$user_id = Session::get('user')['user_id'];
        $where = [['user_id', $user_id]];
        if (!is_null($status)) {
            $where[] = ['item_status', $status];
        }

        if (!empty($request->category)) {
            $items = Relationship::getItems([
                                                ['user_id', $user_id],
                                                ['category_id', $request->category]
                                            ]);

            $Item = Item::where($where)
                        ->whereIn('item_id', $items)
                        ->orderBy('item_order', 'asc')
                        ->get();
        } else if (!empty($request->project)) {
            $items = Includes::getItems([['project_id', $request->project]]);
            $Item = Item::where($where)
                        ->whereIn('item_id', $items)
                        ->orderBy('item_order', 'asc')
                        ->get();
        } else {
            $Item = Item::where($where)
                        ->whereNotIn('item_id', Relationship::getItems([['user_id', $user_id]]))
                        ->whereNotIn('item_id', Includes::getItems([['user_id', $user_id]]))
                        ->orderBy('item_order', 'asc')
                        ->get();
        }

    	$ret = [];
    	$ret['status'] = "OK";
    	$ret['count'] = $Item->count();
    	$ret['data'] = $Item->toArray();
    	if (empty($ret['count'])) {
    		$ret['message'] = "目前尚無任何事項";
    	}
    	return response()->json($ret);
    }

    public function fetchItem($item_id) {
        $ret = [];
        $Item = Item::find($item_id);
        if (empty($Item)) {
            $ret['status'] = "Not Found";
            $ret['message'] = "無此事項";
            return response()->json($ret, 404);
        }
        $ret['status'] = "OK";
        $ret['data'] = $Item->toArray();
        return response()->json($ret);
    }

    public function createItem(Request $request) {
    	$user_id = Session::get('user')['user_id'];
    	$title = $request->title;
    	$content = $request->content;
    	$importance = $request->importance;

        $filter   = [];
        $filter[] = ['item_status', '=', 0];
        $filter[] = ['user_id', '=', $user_id];

        $LastItem = Item::where($filter)
                        ->orderBy('item_order', 'desc')
                        ->first();

    	$Item = new Item;
    	$Item->item_title = $title;
    	$Item->item_content = $content;
        $Item->user_id = $user_id;
    	$Item->item_importance = $importance;
        $Item->item_order = (!empty($LastItem->item_order) ? $LastItem->item_order : 0)  + 1;
    	$Item->create_time = date('Y-m-d H:i:s');

        if (!empty($request->deadline_date)) {
            $Item->deadline_date = $request->deadline_date;
        }

    	$Item->save();

        if (!empty($request->category)) {
            $Relationship = new Relationship;
            $Relationship->user_id = $user_id;
            $Relationship->category_id = $request->category;
            $Relationship->item_id = $Item->item_id;
            $Relationship->create_time = date('Y-m-d H:i:s');
            $Relationship->save();
        } 

        if (!empty($request->project)) {
            $Includes = new Includes;
            $Includes->item_id = $Item->item_id;
            $Includes->user_id = $user_id;
            $Includes->project_id = $request->project;
            $Includes->create_time = date('Y-m-d H:i:s');
            $Includes->save();
        }

    	$ret = [];
    	$ret['status'] = "OK";
    	return response()->json($ret);
    }

    public function updateItem(Request $request) {
    	$user_id = Session::get('user')['user_id'];
    	$item_id = $request->item_id;
    	$title = $request->title;
    	$content = $request->content;
    	$importance = $request->importance;
        if (!empty($request->deadline_date)) {
            $deadline_date = $request->deadline_date;
        }
    	$ret = [];

    	$Item = Item::find($item_id);
    	if (empty($Item)) {
    		$ret['status'] = "None";
    		$ret['message'] = "該事項不存在";
    		return response()->json($ret, 404);
    	}

    	if ($Item->user_id != $user_id) {
    		$ret['status'] = "Forbidden";
    		$ret['message'] = "帳號錯誤";
    		return response()->json($ret, 403);
    	}
    	$Item->item_title = $title;
    	$Item->item_content = $content;
    	$Item->item_importance = $importance;
        if (!empty($deadline_date)) {
            $Item->deadline_date = $deadline_date;
        }
    	$Item->update_time = date('Y-m-d H:i:s');
    	$Item->save();

    	$ret = [];
    	$ret['status'] = "OK";
        $ret['message'] = "修改成功";
    	return response()->json($ret);
    }

    public function processItem(Request $request) {
        $ret = [];
        $item_id = $request->item_id;
        $user_id = Session::get('user')['user_id'];
        $Item = Item::where('item_id', $item_id)
                    ->where('user_id', $user_id)
                    ->first();

        if ($Item) {
            $Item->item_status = 1;
            $Item->start_date = date('Y-m-d');
            $Item->save();

            $ret['status'] = "OK";
            $ret['message'] = "已完成";
            return response()->json($ret);
        }

        $ret['status'] = "Empty";
        $ret['message'] = "該事項不存在項";
        return response()->json($ret, 404);
    }

    public function completeItem(Request $request) {
        $ret = [];
        $item_id = $request->item_id;
        $user_id = Session::get('user')['user_id'];
        $Item = Item::where('item_id', $item_id)
                    ->where('user_id', $user_id)
                    ->first();
        if ($Item) {
            $Item->item_status = 2;
            $Item->complete_date = date('Y-m-d');
            $Item->save();

            $ret['status'] = "OK";
            $ret['message'] = "已完成";
            return response()->json($ret);
        }

        $ret['status'] = "Empty";
        $ret['message'] = "該事項不存在項";
        return response()->json($ret, 404);
    }

    public function refreshItemOrder(Request $request) {
        $failed_item = [];
        $ret = [];
        $item_orders = $request->all();
        DB::beginTransaction();
        foreach ($item_orders as $item_id => $item_order) {
            try {
                $rs = DB::table('item')
                    ->where('item_id', $item_id)
                    ->update(['item_order' => $item_order]);
            } catch (Exception $e) {
                DB::rollback();
                $failed_item[] = $item_id;
            }
        }
        DB::commit();
        if (!empty($failed_item)) {
            $ret['status'] = "Failed";
            $ret['message'] = implode(',', $failed_item) . '修改順序失敗';
            return response()->json($ret, 400);
        }
        $ret['status'] = "OK";
        $ret['message'] = "修改順序成功";
        return response()->json($ret);
    }

    public function deleteItem(Request $request) {
    	$item_id = $request->item_id;

    	$Item = Item::find($item_id);
    	if (empty($Item)) {
    		$ret['status'] = "None";
    		$ret['message'] = "該事項不存在";
    		return response()->json($ret, 404);
    	}
    	$Item->delete();

        $Relationship = Relationship::where('item_id', $item_id)->get();
        if (!empty($Relationship)) {
            foreach ($Relationship as $row) {
                $row->delete();
            }
        }

        $Includes = Includes::where('item_id', $item_id)->get();
        if (!empty($Includes)) {
            foreach ($Includes as $row) {
                $row->delete();
            }
        }

    	$ret = [];
    	$ret['status'] = "OK";
        $ret['message'] = "已刪除";
    	return response()->json($ret);
    }
}
