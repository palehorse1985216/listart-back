<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mail\EmailAuthentication;
use App\Http\Models\Validation;
use App\Http\Models\User;
use Illuminate\Support\Facades\Mail;
use SammyK\LaravelFacebookSdk\LaravelFacebookSdk as LaravelFacebookSdk;
use Google_Client;
use Google_Service_People;
use Twitter;
use Session;

class RestLoginController extends Controller
{
    public function facebook(LaravelFacebookSdk $LaravelFacebookSdk) {
        $callback_url = env('APP_URL') . '/authenticate/facebook';
    	$fb_login_url = $LaravelFacebookSdk->getLoginUrl(['public_profile', 'email'], $callback_url);

    	$ret = [];
    	$ret['status'] = "OK";
    	$ret['url'] = $fb_login_url;
    	return response()->json($ret);
    }

    public function google() {
        $GoogleClient = new Google_Client;
        $GoogleClient->setClientId(env('GOOGLE_CLIENT_ID'));
        $GoogleClient->setClientSecret(env('GOOGLE_CLIENT_SECRET'));
        $GoogleClient->addScope(Google_Service_People::USERINFO_EMAIL);
        $GoogleClient->addScope(Google_Service_People::USERINFO_PROFILE);
        $GoogleClient->setRedirectUri(env('APP_URL') . '/authenticate/google');
        $google_login_url = $GoogleClient->createAuthUrl();

        $ret = [];
        $ret['status'] = "OK";
        $ret['url'] = $google_login_url;
        return response()->json($ret);
    }

    public function twitter() {
        $ret = [];
        $token = Twitter::getRequestToken(route('authenticate-twitter'));
        if (!isset($token['oauth_token']) || 
            !isset($token['oauth_token_secret'])) {
            $ret['status'] = "OK";
            $ret['url'] = route('authenticate-failure', [3, 101]);
            return response()->json($ret);
        }
        $twitter_login_url = Twitter::getAuthorizeURL($token, true, true);

        Session::put('oauth_token', $token['oauth_token']);
        Session::put('oauth_secret', $token['oauth_token_secret']);

        $ret['status'] = "OK";
        $ret['url'] = $twitter_login_url;
        return response()->json($ret);
    }

    public function email(Request $request) {
        $type = 4;
        $ret = [];
        $email = $request->email;
        $uniqid = uniqid('', true);

        $User = User::where([
                                ['user_external_email', '=', $email],
                                ['user_login_type', '=', $type]
                            ])->first();

        if (!empty($User)) {
            $user = [];
            $user['user_id'] = $User->user_id;
            $user['user_external_email'] = $User->user_external_email;
            $user['user_login_type'] = $type;
            Session::put('user', $user);
            $ret['status'] = "OK";
            $ret['url'] = route('login-password');
            return response()->json($ret);
        }

        $Validation = new Validation;
        $Validation->user_email = $email;
        $Validation->validation_code = $uniqid;
        $Validation->create_time = date('Y-m-d H:i:s');
        $Validation->save();

        Mail::to($email)->send(new EmailAuthentication($uniqid));
        $ret['status'] = "OK";
        return response()->json($ret);
    }
}

