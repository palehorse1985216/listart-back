<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Project;
use App\Http\Models\Member;
use App\Http\Models\User;

class RestMemberController extends Controller
{
    public function deleteMember(Request $request) {
    	$ret = [];
    	$Member = Member::find($request->member);
    	if (!empty($Member)) {
    		$data = [];
    		$User = User::find($Member->user_id);
    		$data['member'] = $Member->member_id;
    		$user_name = !empty($User->user_name) ? $User->user_name : (!empty($User->user_nickname) ? $User->user_nickname : '');
			$Project = Project::find($request->project);
    		$Member->delete();
    		$ret['status'] = "OK";
    		$ret['message'] = $user_name . "已退出『" . $Project->project_name . "』";
    		return response()->json($ret);
    	}
    	$ret['status'] = "Member Not Found";
    	$ret['message'] = "專案內無此成員";
    	return response()->json($ret, 404);
    }
}
