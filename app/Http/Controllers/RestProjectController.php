<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\Project;
use App\Http\Models\Member;
use App\Http\Models\Includes;
use App\Http\Models\Item;
use Illuminate\Support\Facades\Mail;
use Session;

class RestProjectController extends Controller
{
    public function getProject() {
		$user_id = Session::get('user')['user_id'];
		$project_ids = Member::getProjectsByUser($user_id);
		if (empty($project_ids)) {
			$ret = [];
			$ret['status'] = "OK";
			$ret['count'] = 0;
			return response()->json($ret);
		}

		$Project = Project::whereIn('project_id', $project_ids)->get();

		$ret = [];
		$ret['status'] = "OK";
		$ret['data'] = $Project->toArray();
		$ret['count'] = $Project->count();
		return response()->json($ret);
	}

	public function fetchProject($project_id) {
		$ret = [];
		$Project = Project::find($project_id);
		if (empty($Project)) {
			$ret['status'] = "Not Found";
			$ret['message'] = "無此分類";
            return response()->json($ret, 404);
		}

		$ret['status'] = "OK";
        $ret['data'] = $Project->toArray();
        return response()->json($ret);
	}

    public function createProject(Request $request) {
    	$name = $request->name;
    	$user_id = Session::get('user')['user_id'];
    	$Project = new Project;
    	$Project->project_name = $name;
    	$Project->create_time = date('Y-m-d H:i:s');
    	$Project->save();

    	$Member = new Member;
    	$Member->user_id = $user_id;
    	$Member->project_id = $Project->project_id;
    	$Member->project_role = 'admin';
    	$Member->create_time = $Project->create_time;
    	$Member->save();

    	$ret = [];
    	$ret['status'] = "OK";
    	$ret['message'] = "建立成功";
    	$ret['project_id'] = $Project->project_id;
    	return response()->json($ret);
    }

    public function updateProject(Request $request) {
    	$project_id = $request->project_id;
    	$name = $request->name;
    	$user_id = Session::get('user')['user_id'];

    	$Project = Project::find($project_id);
    	$Project->project_name = $name;
    	$Project->update_time = date('Y-m-d H:i:s');
    	$Project->save();

    	$ret = [];
    	$ret['status'] = "OK";
    	$ret['message'] = "修改成功";
    	return response()->json($ret);
    }

    public function deleteProject(Request $request) {
        $project_id = $request->project_id;
        $user_id = Session::get('user')['user_id'];

        $Project = Project::find($project_id);
        $Project->delete();

        $Members = Member::where('project_id', $project_id)
                        ->get();

        foreach ($Members as $Member) {
        	$Member->delete();
        }
        
        $to_delete_items = Includes::getItems(['project_id' => $project_id]);

        $Item = Item::whereIn('item_id', $to_delete_items)->get();
        foreach ($Item as $row) {
            $row->delete();
        }

        $Includes = Includes::whereIn('item_id', $to_delete_items)->get();
        foreach ($Includes as $row) {
        	$row->delete();
        }

        $ret = [];
        $ret['status'] = "OK";
        $ret['message'] = "刪除成功";
        return response()->json($ret);
    }
}
