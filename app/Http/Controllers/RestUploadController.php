<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use Storage;
use Session;

class RestUploadController extends Controller
{
    public function avatar(Request $request) {
    	$user_id = Session::get('user')['user_id'];
    	$avatar = $request->avatar;
    	$ext = $avatar->getClientOriginalExtension();
    	$filename = 'avatars/upload/' . $user_id . '-' . time() . '.' . $ext;
		$ret = [];
    	$rs = Storage::disk('public')->put($filename, fopen($avatar, 'r+'));
    	if ($rs) {
    		$url = Storage::url($filename);
    		$User = User::find($user_id);
    		$User->user_avatar = $filename;
    		$User->save();
    		Session::put('user.user_avatar', $url);
    		$ret['status'] = "OK";
    		$ret['url'] = $url;
    	} else {
    		$ret['status'] = "Fail";
    		$ret['message'] = "上傳失敗";
    	}
    	return response()->json($ret);
    }
}
