<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Models\User;
use Session;
use Storage;

class RestUserController extends Controller
{
    public function logout() {
    	$return = [];
    	if (Session::has('user')) {
    		Session::forget('user');
    		$return['status'] = "OK";
    		return response()->json($return);
    	}
    	$return['status'] = "Failed";
    	return response()->json($return);
    }

    public function avatar() {
        $return = [];
        $user_id = Session('user')['user_id'];
        $User = User::find($user_id);
        if (!empty($User)) {
            $return['status'] = "OK";
            $return['avatar'] = Storage::disk('public')
                                       ->url($User->user_avatar);
            return response()->json($return);
        }
        $return['status'] = "Failed";
        $return['message'] = "會員不存在";
        return response()->json($return);
    }

    public function password(Request $request) {
        $new_password = $request->new_password;
        $user_id = Session::get('user')['user_id'];
        User::where('user_id', $user_id)
            ->update(['user_password' => md5($new_password)]);

        $ret = [];
        $ret['status'] = "OK";
        $ret['message'] = "修改成功";
        $ret['url'] = route('item-show');
        return response()->json($ret);
    }

    public function update(Request $request) {
        $user_id = Session::get('user')['user_id'];
        $name = $request->name;
        $nickname = $request->nickname;
        
        $User = User::find($user_id);
        $User->user_name = $name;
        $User->user_nickname = $nickname;
        $User->save();

        $ret = [];
        $ret['status'] = "OK";
        return response()->json($ret);
    }   
}
