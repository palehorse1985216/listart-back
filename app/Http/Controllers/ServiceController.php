<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function term() {
    	$data = [];
    	$data['css'] = 'service-term';
    	$data['js'] = 'service-term';
    	return view('service.term', $data);
    }
}
