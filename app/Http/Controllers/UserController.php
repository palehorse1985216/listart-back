<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use Storage;
use App\Http\Models\User;

class UserController extends Controller
{
    public function setting() {
    	$user_id = Session::get('user')['user_id'];
        $User = User::find($user_id);

        $data = [];
    	$data['title'] = "會員基本資料設定";
        $data['id'] = $user_id;
    	$data['name'] = $User->user_name;
    	$data['email'] = $User->user_external_email;
    	$data['avatar'] = Storage::disk('public')->url($User->user_avatar);
    	$data['nickname'] = !empty($User->user_nickname) ? $User->user_nickname : "";
        $data['js'] = 'user-' . __FUNCTION__;
        $data['css'] = 'user-' . __FUNCTION__;

    	return view('user.setting', $data);
    }

    public function password() {
        $data = [];
        $data['title'] = "設定密碼";
        $data['user_password'] = false;
        $data['js'] = 'user-' . __FUNCTION__;
        $data['css'] = 'user-' . __FUNCTION__;

        $User = User::find(Session::get('user')['user_id']);
        if (!empty($User->user_password)) {
            $data['title'] = "修改密碼";
            $data['user_password'] = true;
        }
        
        return view('user.password', $data);
    }
}
