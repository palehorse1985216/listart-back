<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Http\Models\User;

class AuthenticateUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Session::has('user')) {
            if ($request->ajax()) {
                $ret = [];
                $ret['status'] = "Fail";
                $ret['message'] = "登入逾期";
                $ret['url'] = route('login-show');
                return response()->json($ret, 403);
            }
            return redirect()->route('login-show');
        } else {
            $user_id = Session::get('user')['user_id'];
            $User = User::find($user_id);
            if (empty($User)) {
                if ($request->ajax()) {
                    $ret = [];
                    $ret['status'] = "Fail";
                    $ret['message'] = "帳號不存在";
                    $ret['url'] = route('login-show');
                    return response()->json($ret, 403);
                }
                return redirect()->route('login-show');
            }

            if ($User->user_status != 1) {
                if ($request->ajax()) {
                    $ret = [];
                    $ret['status'] = "Blocked";
                    $ret['message'] = "帳號已被停權";
                    $ret['url'] = env('APP_URL').'/login/show';
                    return response()->json($ret, 403);
                }
                return redirect()->route('authenticate-failure', [$User->login_type, 103]);
            }
        }
        return $next($request);
    }
}
