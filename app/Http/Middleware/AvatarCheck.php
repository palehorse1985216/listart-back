<?php

namespace App\Http\Middleware;

use Closure;

class AvatarCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $avatar = $request->avatar;
        $ext = strtolower($avatar->getClientOriginalExtension());
        //圖片檔案大小(單位：MB)
        $size = (!empty($avatar->getClientSize())) ? round($avatar->getClientSize() / (1024 * 1024), 4) : 0;

        if (!in_array($ext, ['jpg', 'jpeg', 'png', 'gif'])) {
            $ret = [];
            $ret['status'] = "Extension";
            $ret['message'] = "檔案類型須為jpg、jpeg、png或gif";
            return response()->json($ret, 403);
        }

        if (!$size) {
            $ret = [];
            $ret['status'] = "Missing";
            $ret['message'] = "頭像檔案不存在";
            return response()->json($ret, 403);
        }

        if ($size > 5) {
            $ret = [];
            $ret['status'] = "Oversize";
            $ret['message'] = "頭像檔案超過5MB";
            return response()->json($ret, 403);
        }
        return $next($request);
    }
}
