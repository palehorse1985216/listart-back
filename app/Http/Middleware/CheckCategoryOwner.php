<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Http\Models\Category;
use App\Http\Models\Relationship;

class CheckCategoryOwner
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $category_id = $request->category;
        $user_id = Session::get('user')['user_id'];
        $Relationship = Relationship::where('user_id', $user_id)
                                    ->where('category_id', $category_id)
                                    ->get();
        if (empty($Relationship)) {
            if ($request->ajax()) {
                $ret = [];
                $ret['status'] = "Not Owner";
                $ret['message'] = "此分類並不屬於您";
                return response()->json($ret, 400);   
            }
            return false;
        }
        return $next($request);
    }
}
