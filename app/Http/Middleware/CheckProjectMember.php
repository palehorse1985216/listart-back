<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Http\Models\Member;

class CheckProjectMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty($request->project)) {
            $user_id = Session::get('user')['user_id'];
            $Member = Member::where('user_id', $user_id)
                            ->where('project_id', $request->project)
                            ->first();
            if (empty($Member)) {
                if ($request->ajax()) {
                    $ret = [];
                    $ret['status'] = "Not Member";
                    $ret['message'] = "您並非專案的成員";
                    return response()->json($ret, 400);
                }
                return redirect()->route('project-failure', [101]);
            }
        }
        return $next($request);
    }
}
