<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Models\Member;
use Session;

class CheckProjectRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $ret = [];
        $user_id = Session::get('user')['user_id'];
        $Member = Member::where('user_id', $user_id)
                        ->where('project_id', $request->project)
                        ->first();

        $role = $Member->project_role;
        if ($request->ajax()) {
            $action = str_replace('rest/project/', '', $request->path());
        } else {
            $action = str_replace('project/', '', $request->path());
        }
        switch ($action) {
            case 'update':
            case 'delete':
            case 'manage':
                if ($role != 'admin') {
                    if ($request->ajax()) {
                        $ret['status'] = "Permission Denied";
                        $ret['message'] = "您並非專案的管理者";
                        return response()->json($ret, 400);
                    }
                    return redirect()->route('project-failure', [102]);
                }
            break; 
        }
        return $next($request);
    }
}
