<?php

namespace App\Http\Middleware;

use Closure;
use App\Http\Models\User;

class EmailExist
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $User = User::where('user_external_email', $request->user_email)
                    ->first();
        if (!empty($User)) {
            if (!empty($User->user_password)) {
                if ($request->ajax()) {
                    $ret = [];
                    $ret['status'] = "OK";
                    $ret['url'] = route('login-password');
                    return response()->json($ret);
                }
                return redirect('login-password');
            }
        }
        return $next($request);
    }
}
