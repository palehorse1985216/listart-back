<?php

namespace App\Http\Middleware;

use Closure;

class EmailVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $email = $request->email;
        if (!preg_match("/^([\w\-]+)@([\w\-]+\.)?([\w]{2,3})/", $email)) {
            if ($request->ajax()) {
                $ret = [];
                $ret['status'] = "Invalid Email address";
                $ret['message'] = "電子郵件地址格式錯誤";
                return response()->json($ret, 400);
            }
            return redirect()->route('authenticate-failure', [0, 103]);
        }
        return $next($request);
    }
}
