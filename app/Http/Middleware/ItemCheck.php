<?php

namespace App\Http\Middleware;

use Closure;

class ItemCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!empty($request->deadline_date)) {
            $deadline_date = $request->deadline_date;
            $ret = [];
            if (!preg_match('/^\d{4}\-\d{2}\-\d{2}$/', $deadline_date)) {
                $ret['status'] = "Format Error";
                $ret['message'] = "日期格式必須為yyyy-mm-dd";
                return response()->json($ret, 400);
            }

            if (strtotime(date($deadline_date)) < strtotime(date('Y-m-d'))) {
                $ret['status'] = "Deadline Error";
                $ret['message'] = "截止日期必須設定為今日以後的日期";
                return response()->json($ret, 400);
            }

        }
        return $next($request);
    }
}
