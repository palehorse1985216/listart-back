<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Http\Models\User;

class PasswordCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $new_password = $request->new_password;
        $confirm_password = $request->confirm_password;
        $user = Session::get('user');

        if (!empty($request->password)) {
            $User = User::find($user['user_id']);
            if ($User->user_password != md5($request->password)) {
                if ($request->ajax()) {
                    $ret = [];
                    $ret['status'] = "Password Invalid";
                    $ret['message'] = "密碼錯誤";
                    return response()->json($ret, 403);
                }
                return redirect('user-password');
            }
        }

        if ($new_password != $confirm_password) {
            if ($request->ajax()) {
                $ret = [];
                $ret['status'] = "Password Inconsistent";
                $ret['message'] = "密碼不一致";
                return response()->json($ret, 403);
            }
            return redirect('user-password');
        }

        return $next($request);
    }
}
