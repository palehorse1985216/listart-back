<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use App\Http\Models\User;

class PasswordVerify
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $password = md5($request->password);
        $user_id = Session::get('user')['user_id'];
        $User = User::find($user_id);
        if ($password != $User->user_password) {
            if ($request->ajax()) {
                $ret = [];
                $ret['status'] = "Invalid Password";
                $ret['message'] = "密碼錯誤";
                return response()->json($ret, 403);
            }
            return route('login-password');
        }
        return $next($request);
    }
}
