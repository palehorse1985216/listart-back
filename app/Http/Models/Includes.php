<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Includes extends Model
{
    protected $table = 'include';
    protected $primaryKey = "include_id";
    public $timestamps = false;

    public static function getItems($filter = []) {
    	$item_ids = [];
    	$Items = self::where($filter)->get();
    	foreach ($Items as $Item) {
    		$item_ids[] = $Item->item_id;
    	}
    	return $item_ids;
    }
}
