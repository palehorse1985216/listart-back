<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
	protected $table = 'member';
    protected $primaryKey = "member_id";
    public $timestamps = false;

    public static function getProjectsByUser($user_id) {
    	$projects = [];
    	$Members = self::where('user_id', $user_id)
    					 ->get();
    	foreach ($Members as $Member) {
    		$projects[] = $Member->project_id;
    	}
    	return $projects;
    }
}
