<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Relationship extends Model
{
    protected $table = 'relationship';
    protected $primaryKey = "relationship_id";
    public $timestamps = false;

    public static function getItems($filter = []) {
    	$items = [];
    	$relationship = self::where($filter)->get();
    	foreach ($relationship as $row) {
    		$items[] = $row->item_id;
    	}
    	return $items;
    }
}
