<?php

namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

class Validation extends Model
{
    protected $table = 'validation';
    protected $primaryKey = "validation_id";
    public $timestamps = false;
}
