<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Models\User;
use App\Http\Models\Project;

class MemberInvitation extends Mailable
{
    use Queueable, SerializesModels;

    private $_invitor;
    private $_member;
    private $_project;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($invitor, $member, $project)
    {
        $this->_invitor = $invitor;
        $this->_member = $member;
        $this->_project = $project;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $User = User::find($this->_invitor);
        $Project = Project::find($this->_project);
        $data['admin'] = !empty($User->user_name) ? $User->user_name : (!empty($User->user_nickname) ? $User->user_nickname: '');
        $data['project'] = $Project->project_name;
        $data['accept'] = "接受";
        $data['reject'] = "拒絕";
        $data['accept_url'] = env('APP_URL') . "/invite/accept/" . $this->_member . "/" . $this->_project;
        $data['reject_url'] = env('APP_URL') . "/invite/reject/" . $this->_member . "/" . $this->_project;
        return $this->from(env('MAIL_FROM_ADDRESS'))
                    ->markdown('email.invitation')->with($data);
    }
}
