<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('user_id')->unsigned();
            $table->string('user_password', 256)->nullable();
            $table->string('user_name', 125)->nullable();
            $table->string('user_nickname', 125)->nullable();
            $table->string('user_external_id', 64)->nullable();
            $table->string('user_external_email', 125)->nullable();
            $table->string('user_external_token', 256)->nullable();
            $table->text('user_avatar')->nullable();
            $table->unsignedTinyInteger('user_login_type')
                  ->comment('1:Facebook; 2:Google; 3:Instagram; 4:Email;');
            $table->unsignedTinyInteger('user_status')
                  ->comment('0:停用; 1:啟用');
            $table->dateTime('create_time');
            $table->dateTime('update_time')->nullable();
            $table->dateTime('last_login')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user');
    }
}
