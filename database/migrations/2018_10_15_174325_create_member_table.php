<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMemberTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('member', function (Blueprint $table) {
            $table->increments('member_id')->unsigned();
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('project_id');
            $table->string('project_role')
                  ->nullable()
                  ->comment('admin:管理者; member:一般成員');
            $table->unsignedTinyInteger('member_status')
                  ->default(1)
                  ->comment('0:已邀請; 1:參與中; 2:已退出');
            $table->dateTime('create_time');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('member');
    }
}
