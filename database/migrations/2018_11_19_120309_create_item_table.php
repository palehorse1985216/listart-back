<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateItemTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('item', function (Blueprint $table) {
            $table->increments('item_id')->unsigned();
            $table->unsignedInteger('user_id');
            $table->string('item_title', 256);
            $table->text('item_content')->nullable();
            $table->unsignedTinyInteger('item_importance')
                  ->comment('0~10：重要性由低至高');

            $table->unsignedTinyInteger('item_status')
                  ->default(0)
                  ->comment('0:未處理; 1:處理中; 2:已完成');

            $table->unsignedTinyInteger('item_order')
                  ->comment('事項顯示的順序');

            $table->dateTime('repeat_date')
                  ->nullable()
                  ->comment('週期日');

            $table->date('deadline_date')
                  ->nullable()
                  ->comment('截止日期');

            $table->date('start_date')
                  ->nullable()
                  ->comment('開始日期');

            $table->date('complete_date')
                  ->nullable()
                  ->comment('完成日期');
                  
            $table->dateTime('create_time');
            $table->dateTime('update_time')->nullable();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('item');
    }
}
