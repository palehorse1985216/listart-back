module.exports = (function() {
	var selectdown = require('kafeore-selectmenu/kafeore-selectmenu.jquery.js'),
		Ajax = require('mandeling-ajax/mandeling-ajax.js'), 
		logoutAJAX = new Ajax(), 
		ImageAJAX = new Ajax(),
		headerVue;
		

	$(document).ready(function() {
		headerVue = new Vue({
						el: '#page-header',
						methods: {
							logout: function(event) {
								event.preventDefault();
								logoutAJAX.get('/rest/user/logout')
										  .success(function(res) {
											if (res.status == "OK") {
												location.href = "/login/show";
											}
										  });
							} 
						}
					});

		if ($('#user-select-menu').length) {
			$('#user-select-menu').selectmenu({
				mode: 'fade',
				latency: 200,
			});
		}
	});
		

	$(window).on('load', function(event) {
		ImageAJAX.get('/rest/user/avatar')
				 .success(function(res) {
				 	var imageContainer = new Image();

					imageContainer.src = res.avatar;
					imageContainer.onload = function() {
						$('#user-select-menu').css({
							backgroundImage: 'url("' + res.avatar + '")'
						});
						$('#user-select-menu .header-item-cover').fadeOut(120);
					}
				 });
		
	});
})();