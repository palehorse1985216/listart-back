var tab = require('brazilian-tabs/brazilian-tabs.jquery.js'),
	dialog = require('baguette-dialog/baguette-dialog.jquery.js'),
	Ajax = require('mandeling-ajax/mandeling-ajax.js'),
	UriParameters = require('uri-parameters/uri-parameters.js'),
	validate = require('burendo-validator/burendo-validator.jquery.js'),
	adjustment = require('arabica-adjustment/arabica-adjustment.jquery.js'),
	darg2sortable = require('drag2sortable/drag2sortable.jquery.js'),
	datetimepicker = require('clivia-datepicker/clivia-datepicker.jquery.js'),
	editor = require('./library/lightext-editor.jquery.js'),
	getCategoryAJAX = new Ajax(),
	fetchCategoryAJAX = new Ajax(),
	createCategoryAJAX = new Ajax(),
	updateCategoryAJAX = new Ajax(),
	deleteCategoryAJAX = new Ajax(),
	getItemsAJAX = new Ajax(),
	fetchItemAJAX = new Ajax(),
	createItemAJAX = new Ajax(),
	updateItemAJAX = new Ajax(),
	processItemAJAX = new Ajax(),
	completeItemAJAX = new Ajax(),
	deleteItemAJAX = new Ajax(),
	contents = {}, duration = 300, seconds = 1200, selectedItem,
	category_id = (new UriParameters()).get('category') ? (new UriParameters()).get('category') : 0;

$(document).ready(function() {
	$('.item-dialog .item-edit-form .btn-item-process').on('click', function(e) {
		e.preventDefault();
		processItemAJAX.get('/rest/csrf/refresh')
					   .success(function(resCSRF) {
							var item_id = $('.item-dialog .item-edit-form #item-id').val();
							processItemAJAX.post('/rest/item/process', {item_id: item_id}, resCSRF.csrf)
								.success(function(res) {
									if (res.status === "OK") {
										$('.item-dialog').dialog('hide', function() {
											$('.item-dialog .item-edit-form .btn-item-process').hide();
											$('.item-dialog .item-edit-form').validate('resolve');

											$('.item-dialog .item-edit-form').find('input')
																			 .val('')

											$('.item-dialog .item-edit-form').find('textarea')
																			 .val('');
											$('.item-dialog #item-deadline').datepicker('reset')
																			.datepicker('hide');
											$('.item-dialog #item-importance').adjustment('reset');
											$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
											$('ul.list-tab #todo-item').click();
										});
									}
								});
						}).fail(function(resCSRF) {
							if (typeof resCSRF.message === 'string') {
								$('.item-dialog .item-edit-form .item-message .response-message').text(resCSRF.message);
							} else {
								$('.item-dialog .item-edit-form .item-message .response-message').text("登入逾期");
							}
							
							$('.item-dialog .item-edit-form .item-message').animate({
								'opacity': 1
							}, duration, function() {
								window.setTimeout(function() {
									$('.item-dialog').dialog('hide', function() {
										if (typeof resCSRF.url === 'string') {
											location.href = resCSRF.url;
											return false;
										}
										$('.item-dialog .item-edit-form .btn-item-process').hide();
										$('.item-dialog .item-edit-form').validate('resolve');

										$('.item-dialog .item-edit-form').find('input')
																		 .val('')

										$('.item-dialog .item-edit-form').find('textarea')
																		 .val('');
										$('.item-dialog #item-deadline').datepicker('reset')
																		.datepicker('hide');;
										$('.item-dialog #item-importance').adjustment('reset');
										$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
									});
								}, seconds);
							});
						});
	});

	$('.item-dialog .item-edit-form .btn-item-complete').on('click', function(e) {
		e.preventDefault();
		completeItemAJAX.get('/rest/csrf/refresh')
						.success(function(resCSRF) {
							var item_id = $('.item-dialog .item-edit-form #item-id').val();
							completeItemAJAX.post('/rest/item/complete', {item_id: item_id}, resCSRF.csrf)
											.success(function(res) {
												if (res.status === "OK") {
													$('.item-dialog').dialog('hide', function() {
														$('.item-dialog .item-edit-form .btn-item-process').hide();
														$('.item-dialog .item-edit-form').validate('resolve');

														$('.item-dialog .item-edit-form').find('input')
																						 .val('')

														$('.item-dialog .item-edit-form').find('textarea')
																						 .val('');
														$('.item-dialog #item-deadline').datepicker('reset')
																						.datepicker('hide');
														$('.item-dialog #item-importance').adjustment('reset');
														$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
														$('ul.list-tab #progress-item').click();
													});
												}
											});
						}).fail(function(resCSRF) {
							if (typeof resCSRF.message === 'string') {
								$('.item-dialog .item-edit-form .item-message .response-message').text(resCSRF.message);
							} else {
								$('.item-dialog .item-edit-form .item-message .response-message').text("登入逾期");
							}
							
							$('.item-dialog .item-edit-form .item-message').animate({
								'opacity': 1
							}, duration, function() {
								window.setTimeout(function() {
									$('.item-dialog').dialog('hide', function() {
										if (typeof resCSRF.url === 'string') {
											location.href = resCSRF.url;
											return false;
										}
										$('.item-dialog .item-edit-form .btn-item-process').hide();
										$('.item-dialog .item-edit-form').validate('resolve');

										$('.item-dialog .item-edit-form').find('input')
																		 .val('')

										$('.item-dialog .item-edit-form').find('textarea')
																		 .val('');
										$('.item-dialog #item-deadline').datepicker('reset')
																		.datepicker('hide');
										$('.item-dialog #item-importance').adjustment('reset');
										$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
									});
								}, seconds);
							});
						});
	});

	$('body').delegate('.category-item', 'click', function(e) {
		e.preventDefault();
		e.stopPropagation();
	});

	$('body').delegate('.category-item', 'mousedown touchstart', function(e) {
		e.preventDefault();
		e.stopPropagation();
		if (e.button > 0) return;
		var categoryItem = $(this);
		categoryItem.data('mousedown-time', Date.now())
			   		.data('mousedown', true);
		window.setTimeout(function() {
			if (categoryItem.data('mousedown')) {
				fetchCategoryAJAX.get('/rest/category/fetch/' + categoryItem.data('category-id'))
								 .success(function(res) {
								 	$('.category-dialog .category-edit-form #category-name').val(res.data.category_name);
								 	$('.category-dialog .category-edit-form #category-id').val(res.data.category_id);
								 	$('.category-dialog .category-edit-form .btn-category-update').off('click').on('click', function(e) {
								 		e.preventDefault();
								 		updateCategoryAJAX.get('/rest/csrf/refresh')
								 						  .success(function(resCSRF) {
								 						  	  var data = {};
								 						  	  data.category_id = $('.category-dialog .category-edit-form #category-id').val();
								 						  	  data.name = $('.category-dialog .category-edit-form #category-name').val();
								 						  	  updateCategoryAJAX.post('/rest/category/update', data, resCSRF.csrf)
								 						  	  					.success(function(res) {
								 						  	  						categoryItem.find('span.category-item-title').text(data.name);
								 						  	  						if (typeof res.message === 'string') {
								 						  	  							$('.category-dialog .category-edit-form .category-message .response-message').text(res.message);
								 						  	  						} else {
								 						  	  							$('.category-dialog .category-edit-form .category-message .response-message').text("修改成功");
								 						  	  						}

								 						  	  						$('.category-dialog .category-edit-form .category-message').animate({opacity: 1}, duration, function() {
								 						  	  							window.setTimeout(function() {
									 						  	  							$('.category-dialog .category-edit-form .category-message').animate({opacity: 0}, duration, function() {
									 						  	  								$('.category-dialog').dialog('hide', function() {
									 						  	  									$('.category-dialog .category-edit-form #category-name').val('');
									 						  	  									$('.category-dialog .category-edit-form #category-id').val('');
									 						  	  								});
									 						  	  							});
									 						  	  						}, seconds);
								 						  	  						});
								 						  	  						
								 						  	  					}).fail(function(res) {
								 						  	  						if (typeof res.message === 'string') {
								 						  	  							$('.category-dialog .category-edit-form .category-message .response-message').text(res.message);
								 						  	  						} else {
								 						  	  							$('.category-dialog .category-edit-form .category-message .response-message').text("修改失敗");
								 						  	  						}

								 						  	  						$('.category-dialog .category-edit-form .category-message').animate({opacity: 1}, duration, function() {
								 						  	  							window.setTimeout(function() {
									 						  	  							$('.category-dialog .category-edit-form .category-message').animate({opacity: 0}, duration, function() {
									 						  	  								$('.category-dialog').dialog('hide', function() {
									 						  	  									if (typeof res.url === 'string') {
											 						  	  								location.href = res.url;
											 						  	  								return;
											 						  	  							}

											 						  	  							$('.category-dialog .category-edit-form #category-name').val('');
										 						  	  								$('.category-dialog .category-edit-form #category-id').val('');
									 						  	  								});
									 						  	  							});
									 						  	  						}, seconds);
								 						  	  						});
								 						  	  						
								 						  	  					});
								 						  }).fail(function(resCSRF) {
								 						  		if (typeof resCSRF.message === 'string') {
			 						  	  							$('.category-dialog .category-edit-form .category-message .response-message').text(resCSRF.message);
			 						  	  						} else {
			 						  	  							$('.category-dialog .category-edit-form .category-message .response-message').text("修改失敗");
			 						  	  						}

			 						  	  						$('.category-dialog .category-edit-form .category-message').animate({opacity: 1}, duration, function() {
			 						  	  							window.setTimeout(function() {
				 						  	  							$('.category-dialog .category-edit-form .category-message').animate({opacity: 0}, duration, function() {
				 						  	  								$('.category-dialog').dialog('hide', function() {
				 						  	  									if (typeof resCSRF.url === 'string') {
						 						  	  								location.href = resCSRF.url;
						 						  	  								return;
						 						  	  							}

						 						  	  							$('.category-dialog .category-edit-form #category-name').val('');
					 						  	  								$('.category-dialog .category-edit-form #category-id').val('');
				 						  	  								});
				 						  	  							});
				 						  	  						}, seconds);
			 						  	  						});
								 						  });
								 					});

									$('.category-dialog .category-edit-form .btn-category-delete').off('click').on('click', function(e) {
										e.preventDefault();
										var data = {};
										data.category_id = $('.category-dialog .category-edit-form #category-id').val();
										$('.category-dialog').dialog('hide', function() {
											$('.category-dialog .category-edit-form #category-name').val('');
											$('.category-dialog .category-edit-form #category-id').val('');
											
											$('.confirm-dialog .confirm-dialog-content .confirm-dialog-content-text').text("是否連同相關事項一併刪除？");
											$('.confirm-dialog').dialog('show', function() {
												$('.confirm-dialog').dialog('dismiss');
												$('.confirm-dialog .confirm-dialog-header .confirm-dialog-close').on('click', function(e) {
													$('.confirm-dialog').dialog('hide');
												});
												
												$('.confirm-dialog .confirm-dialog-footer button').off('click').on('click', function(e) {
													data.is_items_deleted = 0;
													if ($(this).is('.confirm-dialog-affirmative')) {
														data.is_items_deleted = 1;
													}

													deleteCategoryAJAX.get('/rest/csrf/refresh')
																	  .success(function(resCSRF) {
																	  	  deleteCategoryAJAX.post('/rest/category/delete', data, resCSRF.csrf)
																	  	  					.success(function(res) {
																	  	  						$('.category-bar .category-item').each(function() {
																	  	  							if ($(this).data('category-id') == data.category_id) {
																	  	  								$(this).remove();
																	  	  								return false;
																	  	  							}
																	  	  						});

																						  	    if (typeof res.message === 'string') {
																							  	  	$('.message-dialog .message-dialog-content .message-dialog-content-text').text(res.message);
																							  	} else {
																							  	  	$('.message-dialog .message-dialog-content .message-dialog-content-text').text("刪除成功");
																							  	}

																							  	$('.confirm-dialog').dialog('hide', function() {
																							  		$('.message-dialog .message-dialog-header .message-dialog-close').hide();
																							  		$('.message-dialog').dialog('show', function() {
																								  		window.setTimeout(function() {
																								  			$('.message-dialog').dialog('hide', function() {
																								  				$('.message-dialog .message-dialog-header .message-dialog-close').show();
																								  			});
																								  		}, seconds)
																								  	});
																							  	});
																	  	  					}).fail(function(res) {
																	  	  						if (typeof res.message === 'string') {
																							  	  	$('.message-dialog .message-dialog-content .message-dialog-content-text').text(res.message);
																							  	} else {
																							  	  	$('.message-dialog .message-dialog-content .message-dialog-content-text').text("刪除失敗");
																							  	}
																							  	
																							  	$('.confirm-dialog').dialog('hide', function() {
																							  		$('.message-dialog .message-dialog-header .message-dialog-close').hide();
																							  		$('.message-dialog').dialog('show', function() {
																								  		window.setTimeout(function() {
																								  			$('.message-dialog').dialog('hide', function() {
																								  				$('.message-dialog .message-dialog-header .message-dialog-close').show();
																								  			});
																								  		}, seconds)
																								  	});
																							  	});
																	  	  					});
																	  }).fail(function(resCSRF) {
																	  	if (typeof res.message === 'string') {
																	  	  	$('.message-dialog .message-dialog-content .message-dialog-content-text').text(res.message);
																	  	} else {
																	  	  	$('.message-dialog .message-dialog-content .message-dialog-content-text').text("登入逾期");
																	  	}
																	  	
																	  	$('.confirm-dialog').dialog('hide', function() {
																	  		$('.message-dialog .message-dialog-header .message-dialog-close').hide();
																	  		$('.message-dialog').dialog('show', function() {
																		  		window.setTimeout(function() {
																		  			$('.message-dialog').dialog('hide', function() {
																		  				if (typeof resCSRF.url === 'string') {
																		  					location.href = resCSRF.url;
																		  					return;
																		  				}
																		  				$('.message-dialog .message-dialog-header .message-dialog-close').show();
																		  			});
																		  		}, seconds)
																		  	});
																	  	});
																	  });
												});
												
											});
										});
									});

								 }).fail(function(res) {
								 	if (typeof res.message === 'string') {
								 		$('.category-dialog .category-message .response-message').text(res.message);
								 	} else {
								 		$('.category-dialog .category-message .response-message').text("無資料");
								 	}
								 	$('.category-dialog .category-message').animate({opacity: 1}, duration);
								 	window.setTimeout(function() {
								 		$('.category-dialog .category-message').animate({opacity: 0}, duration);
								 		if (typeof res.url === 'string') {
								 			$('.category-dialog').dialog('hide', function() {
								 				location.href = res.url;
								 			});
								 		}
								 	}, seconds);
								 });
				
				if (category_id == categoryItem.data('category-id')) {
					$('.category-dialog .category-edit-form .btn-category-delete').hide();
				} else {
					$('.category-dialog .category-edit-form .btn-category-delete').show();
				}
				
				$('.category-dialog').dialog('show', function() {
					$('.category-dialog').dialog('dismiss', function() {
						$('.category-dialog .category-edit-form #category-name').val('');
					});
					
					$('.category-dialog .category-dialog-header .category-dialog-close').on('click', function() {
						$('.category-dialog').dialog('hide', function() {
							$('.category-dialog .category-edit-form #category-name').val('');
						});
					});
				});
			}
		}, 1000);

	});

	$('body').delegate('.category-item', 'mouseup touchend', function(e) {
		e.preventDefault();
		e.stopPropagation();
		var categoryItem = $(this);
		categoryItem.data('mousedown', false);
		if (Date.now() - categoryItem.data('mousedown-time') < 1000) location.href = typeof categoryItem.data('category-id') !== 'undefined' ? (new UriParameters('/item/show')).build({'category': categoryItem.data('category-id')}).getUrl() : '/item/show';
	});

	$('body').delegate('.list-item-delete', 'click', function(e) {
		e.preventDefault();
		var data = {}, item = $(this).prev('.list-single-item');
		$('.confirm-dialog .confirm-dialog-content .confirm-dialog-content-text').text("確定刪除『" + item.find('.list-item-title').text() + "』？");
		$('.confirm-dialog').dialog('show');
		
		$('.confirm-dialog .confirm-dialog-footer .confirm-dialog-negative').off('click').on('click', function(e) {
			e.preventDefault();
			$('.confirm-dialog').dialog('hide');
		});

		$('.confirm-dialog .confirm-dialog-footer .confirm-dialog-affirmative').off('click').on('click', function(e) {
			e.preventDefault();
			$('.confirm-dialog').dialog('hide', function() {
				deleteItemAJAX.get('/rest/csrf/refresh')
							  .success(function(resCSRF) {
									data.item_id = item.data('item-id');
									deleteItemAJAX.post('/rest/item/delete', data, resCSRF.csrf)
												  .success(function(res) {
														if (typeof res.message === 'string') {
															$('.message-dialog .message-dialog-content .message-dialog-content-text').text(res.message);
														} else {
															$('.message-dialog .message-dialog-content .message-dialog-content-text').text('已刪除');
														}

														$('.message-dialog .message-dialog-close').hide();
														$('.message-dialog').dialog('show', function() {
															window.setTimeout(function() {
																$('.message-dialog').dialog('hide', function() {
																	$('ul.list-tab #' + selectedItem).click();
																	$('.message-dialog .message-dialog-close').show();
																});
															}, seconds);
														});
													}).fail(function(res) {
														if (typeof res.message === 'string') {
															$('.message-dialog .message-dialog-content .message-dialog-content-text').text(res.message);
														} else {
															$('.message-dialog .message-dialog-content .message-dialog-content-text').text('刪除失敗');
														}

														$('.message-dialog .message-dialog-close').hide();
														$('.message-dialog').dialog('show', function() {
															window.setTimeout(function() {
																$('.message-dialog').dialog('hide', function() {
																	$('.message-dialog .message-dialog-close').show();
																});
															}, seconds);
														});
													});
								}).fail(function(resCSRF) {
									if (typeof resCSRF.message === 'string') {
										$('.message-dialog .message-dialog-content .message-dialog-content-text').text(resCSRF.message);
										$('.message-dialog').dialog('show');
										window.setTimeout(function() {
											$('.message-dialog').dialog('hide', function() {
												if (typeof resCSRF.url === 'string') {
													location.href = resCSRF.url;
													return false;
												}
											});
										}, seconds);
									} else {
										if (typeof resCSRF.url === 'string') {
											location.href = resCSRF.url;
											return false;
										}
									}
								});
			});
		});
	});

	$('body').delegate('.list-item-edit', 'click', function(e) {									
		e.preventDefault();
		e.stopPropagation();
		var item = $(this).next('li.list-single-item');
		fetchItemAJAX.get('/rest/item/fetch/' + item.data('item-id'))
					 .success(function(resFetch) {
						switch (selectedItem) {
							case 'progress-item':
								$('.item-dialog .item-edit-form .btn-item-process').hide();
								$('.item-dialog .item-edit-form .btn-item-complete').show();
								break;
							case 'todo-item':
								$('.item-dialog .item-edit-form .btn-item-process').show();
								$('.item-dialog .item-edit-form .btn-item-complete').hide();
								break;
						}
						$('.item-dialog .item-edit-form .btn-item-delete').show();
						$('.item-dialog').dialog('show', function() {
							$('.item-dialog .item-edit-form #item-title').val(resFetch.data.item_title);
							$('.item-dialog .item-edit-form #item-content').val(resFetch.data.item_content);
							$('.item-dialog .item-edit-form #item-id').val(resFetch.data.item_id);
							if (resFetch.data.deadline_date) {
								$('.item-dialog .item-edit-form #item-deadline').datepicker('set', resFetch.data.deadline_date);
							}
							$('.item-dialog .item-edit-form #item-importance').adjustment('set', resFetch.data.item_importance);
						});

						$('.item-dialog .item-edit-form').validate({
							format: {
								date: /^20\d{2}\-\d{2}\-\d{2}$/
							},
							message: {
								format: {
									date: "日期格式為 yyyy-mm-dd"
								}
							},
							success: function() {
								var data = {};
								data.item_id = $('.item-dialog .item-edit-form #item-id').val();
								data.title = $('.item-dialog .item-edit-form #item-title').val();
								if ($('.item-dialog .item-edit-form #item-content').val() != '') {
									data.content = $('.item-dialog .item-edit-form #item-content').val();
								}

								if ($('.item-dialog .item-edit-form #item-deadline').val()) {
									data.deadline_date = $('.item-dialog .item-edit-form #item-deadline').val();
								}

								data.importance = $('.item-dialog .item-edit-form #item-importance').val();

								updateItemAJAX.get('/rest/csrf/refresh')
											  .success(function(resCSRF) {
													updateItemAJAX.post('/rest/item/update', data, resCSRF.csrf)
																  .success(function(res) {
																		if (typeof res.message === 'string') {
																			$('.item-dialog .item-edit-form .item-message .response-message').text(res.message);
																		} else {
																			$('.item-dialog .item-edit-form .item-message .response-message').text('修改成功');
																		}

																		$('.item-dialog .item-edit-form .item-message').animate({
																			'opacity': 1
																		}, duration, function() {
																			window.setTimeout(function() {
																				$('.item-dialog').dialog('hide', function() {
																					if (typeof resCSRF.url === 'string') {
																						location.href = resCSRF.url;
																						return false;
																					}
																					$('.item-dialog .item-edit-form .btn-item-process').hide();
																					$('.item-dialog .item-edit-form').validate('resolve');

																					$('.item-dialog .item-edit-form').find('input')
																													 .val('')

																					$('.item-dialog .item-edit-form').find('textarea')
																													 .val('');
																					$('.item-dialog #item-deadline').datepicker('reset')
																													.datepicker('hide');			 
																					$('.item-dialog #item-importance').adjustment('reset');
																					$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
																					$('ul.list-tab #todo-item').click();
																				});
																			}, seconds);
																		});
																	}).fail(function(res) {
																		if (typeof res.message === 'string') {
																			$('.item-dialog .item-edit-form .item-message .response-message').text(res.message);
																		} else {
																			$('.item-dialog .item-edit-form .item-message .response-message').text("修改失敗");
																		}
																		$('.item-dialog .item-edit-form .item-message').animate({
																			'opacity': 1
																		}, duration, function() {
																			window.setTimeout(function() {
																				if (typeof res.url === 'string') {
																					$('.item-dialog').dialog('hide', function() {
																						$('.item-dialog .item-edit-form .btn-item-process').hide();
																						$('.item-dialog .item-edit-form').validate('resolve');

																						$('.item-dialog .item-edit-form').find('input')
																														 .val('')

																						$('.item-dialog .item-edit-form').find('textarea')
																														 .val('');
																						$('.item-dialog #item-deadline').datepicker('reset')
																														.datepicker('hide');
																						$('.item-dialog #item-importance').adjustment('reset');
																						
																						$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
																						location.href = res.url;
																						return false;
																					});
																				} else {
																					$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
																				}
																			}, seconds);
																		});
																	});
												})
												.fail(function(resCSRF) {
													if (typeof resCSRF.message === 'string') {
														$('.item-dialog .item-edit-form .item-message .response-message').text(resCSRF.message);
														$('.item-dialog .item-edit-form .item-message').animate({
															'opacity': 1
														}, duration, function() {
															window.setTimeout(function() {
																$('.item-dialog').dialog('hide', function() {
																	if (typeof resCSRF.url === 'string') {
																		location.href = resCSRF.url;
																		return false;
																	}
																	$('.item-dialog .item-edit-form .btn-item-process').hide();
																	$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
																});
															}, seconds);
														});
													} else {
														$('.item-dialog').dialog('hide', function() {
															if (typeof resCSRF.url === 'string') {
																location.href = resCSRF.url;
																return false;
															}
															$('.item-dialog .item-edit-form .btn-item-process').hide();
															$('.item-dialog .item-edit-form .item-message').hide();
														});
													}
												});
							}
						});

				$('.item-dialog').dialog('dismiss', function() {
					$('.item-dialog .item-edit-form .btn-item-process').hide();
					$('.item-dialog .item-edit-form').find('input[type=text],textarea')
						 							 .val('')
						 							 .validate('resolve');
					$('.item-dialog .item-edit-form #item-deadline').datepicker('reset')
																	.datepicker('hide');
					$('.item-dialog .item-edit-form #item-importance').adjustment('reset');
				});
			}).fail(function(res) {
				if (typeof res.message === 'string') {
					$('.message-dialog .message-dialog-content .message-dialog-content-text').text(res.message);
					$('.message-dialog').dialog('show');
					$('.message-dialog').dialog('dismiss', function() {
						if (typeof res.url === 'string') {
							location.href = res.url;
							return;
						}
						$('.item-dialog .item-edit-form .btn-item-process').hide();
					});

					$('.message-dialog .message-dialog-close').on('click', function(e) {
						e.preventDefault();
						$('.message-dialog').dialog('hide', function() {
							if (typeof res.url === 'string') {
								location.href = res.url;
								return;
							}
							$('.item-dialog .item-edit-form .btn-item-process').hide();
						});
						$('.message-dialog .message-dialog-content .message-dialog-content-text').text('');
					});
				}
				if (typeof res.url === 'string') {
					location.href = res.url;
					return;
				}
			});
	});
	
	$('.category-dialog').dialog();
	$('.btn-add-new-category').on('click', function(e) {
		e.preventDefault();
		$('.category-dialog .category-edit-form .btn-category-delete').hide();
		$('.category-dialog').dialog('show');
		$('.category-dialog').dialog('dismiss');
		$('.category-dialog .category-edit-form .btn-category-update').off('click').on('click', function(e) {
			e.preventDefault();
			createCategoryAJAX.get('/rest/csrf/refresh')
							  .success(function(resCSRF) {
									var data = {};
									data.name = $('.category-dialog .category-edit-form #category-name').val();
									createCategoryAJAX.post('/rest/category/create', data, resCSRF.csrf)
													  .success(function(res) {
															if (typeof res.message === 'string') {
																$('.category-dialog .category-edit-form .category-message .response-message').text(res.message);
																$('.category-dialog .category-edit-form .category-message').animate({
																	'opacity': 1
																}, duration, function() {
																	window.setTimeout(function() {
																		$('.category-dialog').dialog('hide', function() {
																			location.href = '/item/show?category=' + res.category_id;
																			return false;
																		});
																	}, seconds);
																});
															} else {
																$('.category-dialog').dialog('hide', function() {
																	location.href = '/item/show?category=' + res.category_id;
																	return false;
																});
															}

															getCategoryAJAX.get('/rest/category/get')
																		   .success(function(res) {
																				$('.category-bar .category-item').not('.btn-add-new-category').remove();
																				for (var i in res.data) {
																					var category = $('<button class="btn btn-small btn-info category-item"><i class="fa fa-tag category-item-icon"></i><span class="category-item-title">' + res.data[i].category_name + '</span></button>');
																					
																					if (res.data[i].category_id == category_id) {
																						category.addClass('btn-info')
																								.addClass('category-item-selected');
																					} else {
																						category.addClass('btn-light');
																					}

																					category.data('category-id', res.data[i].category_id);
																					category.appendTo('.category-bar');
																				}
																				
																				if (!$('.category-bar .btn-info').length) {
																					$('.category-bar .no-category').removeClass('btn-light')
																												   .addClass('btn-info')
																												   .addClass('.category-item-selected');
																				}
																			});
														}).fail(function(res) {
															if (typeof res.message === 'string') {
																$('.category-dialog .category-edit-form .category-message .response-message').text(res.message);
																$('.category-dialog .category-edit-form .category-message').animate({
																	'opacity': 1
																}, duration, function() {
																	window.setTimeout(function() {
																		$('.category-dialog').dialog('hide', function() {
																			if (typeof res.url === 'string') {
																				location.href = res.url;
																				return false;
																			}
																			$('.category-dialog .category-edit-form input').val('');
																			$('.category-dialog .category-edit-form .category-message').animate({'opacity': 0}, duration)
																		});
																	}, seconds);
																});
															} else {
																$('.category-dialog').dialog('hide', function() {
																	if (typeof res.url === 'string') {
																		location.href = res.url;
																		return false;
																	}
																	$('.category-dialog .category-edit-form input').val('');
																});
															}
														});
								}).fail(function(resCSRF) {
									if (typeof resCSRF.message === 'string') {
										$('.category-dialog .category-edit-form .category-message .response-message').text(resCSRF.message);
										$('.category-dialog .category-edit-form .category-message').animate({
											'opacity': 1
										}, duration, function() {
											window.setTimeout(function() {
												$('.category-dialog').dialog('hide', function() {
													$('.category-dialog .category-edit-form .btn-category-delete').show();
													if (typeof resCSRF.url === 'string') {
														location.href = resCSRF.url;
														return false;
													}
													$('.category-dialog .category-edit-form .category-message').animate({'opacity': 0}, duration)
												});
											}, seconds);
										});
									} else {
										$('.category-dialog').dialog('hide', function() {
											$('.category-dialog .category-edit-form .btn-category-delete').show();
											if (typeof resCSRF.url === 'string') {
												location.href = resCSRF.url;
												return false;
											}
											$('.category-dialog .category-edit-form .category-message').hide();
										});
									}
								});
			});
		});

		$('.category-dialog .category-dialog-header .category-dialog-close').on('click', function(e) {
			$('.category-dialog').dialog('hide');
		});

	$('.item-dialog').dialog();
	$('.item-dialog .item-edit-form #item-content').editor({fontSize: 16, rows: 5});

	$('.message-dialog').dialog();
	$('.confirm-dialog').dialog();
	$('.add-new-item, .mobile-add-new-item').on('click touchstart', function(e) {
		e.preventDefault();
		e.stopPropagation();

		$('.item-dialog .item-edit-form .btn-item-process').hide();
		$('.item-dialog .item-edit-form .btn-item-complete').hide();
		$('.item-dialog .item-edit-form .btn-item-delete').hide();

		$('.item-dialog .item-edit-form').validate({
			format: {
				date: /^20\d{2}\-\d{2}\-\d{2}$/
			},
			message: {
				format: {
					date: "日期格式為 yyyy-mm-dd"
				}
			},
			success: function() {
				var data = {};

				data.title = $('.item-dialog .item-edit-form #item-title').val();
				
				if ($('.item-dialog .item-edit-form #item-deadline').val()) {
					data.deadline_date = $('.item-dialog .item-edit-form #item-deadline').val();
				}

				if ($('.item-dialog .item-edit-form #item-content').val() != '') {
					data.content = $('.item-dialog .item-edit-form #item-content').val();
				}
				data.importance = $('.item-dialog .item-edit-form #item-importance').val();

				createItemAJAX.get('/rest/csrf/refresh')
							  .success(function(resCSRF) {
							  		var url = '/rest/item/create';
							  		if (category_id) {
							  			url += '?category=' + category_id;
							  		}
									createItemAJAX.post(url, data, resCSRF.csrf)
												  .success(function(res) {
														if (typeof res.message === 'string') {
															$('.item-dialog .item-edit-form .item-message .response-message').text(res.message);
														} else {
															$('.item-dialog .item-edit-form .item-message .response-message').text('建立成功');
														}

														$('.item-dialog .item-edit-form .item-message').animate({
															'opacity': 1
														}, duration, function() {
															window.setTimeout(function() {
																$('.item-dialog').dialog('hide', function() {
																	if (typeof resCSRF.url === 'string') {
																		location.href = resCSRF.url;
																		return false;
																	}
																	$('.item-dialog .item-edit-form').validate('resolve');

																	$('.item-dialog .item-edit-form').find('input')
																									 .val('')

																	$('.item-dialog .item-edit-form').find('textarea')
																									 .val('');
																	$('.item-dialog #item-deadline').datepicker('reset')
																									.datepicker('hide');
																	$('.item-dialog #item-importance').adjustment('reset');
																	$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
																	$('ul.list-tab #todo-item').click();
																});
															}, seconds);
														});
													}).fail(function(res) {
														if (typeof res.message === 'string') {
															$('.item-dialog .item-edit-form .item-message .response-message').text(res.message);
														} else {
															$('.item-dialog .item-edit-form .item-message .response-message').text("修改失敗");
														}
														$('.item-dialog .item-edit-form .item-message').animate({
															'opacity': 1
														}, duration, function() {
															window.setTimeout(function() {
																if (typeof res.url === 'string') {
																	$('.item-dialog').dialog('hide', function() {
																		$('.item-dialog .item-edit-form .btn-item-process').hide();
																		$('.item-dialog .item-edit-form').validate('resolve');

																		$('.item-dialog .item-edit-form').find('input')
																										 .val('')

																		$('.item-dialog .item-edit-form').find('textarea')
																										 .val('');
																		$('.item-dialog #item-deadline').datepicker('reset')
																										.datepicker('hide');
																		$('.item-dialog #item-importance').adjustment('reset');
																		
																		$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
																		location.href = res.url;
																		return false;
																	});
																} else {
																	$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration);
																}
															}, seconds);
														});
													});
								})
								.fail(function(resCSRF) {
									if (typeof resCSRF.message === 'string') {
										$('.item-dialog .item-edit-form .item-message .response-message').text(resCSRF.message);
										$('.item-dialog .item-edit-form .item-message').animate({
											'opacity': 1
										}, duration, function() {
											window.setTimeout(function() {
												$('.item-dialog').dialog('hide', function() {
													if (typeof resCSRF.url === 'string') {
														location.href = resCSRF.url;
														return false;
													}
													$('.item-dialog .item-edit-form .item-message').animate({'opacity': 0}, duration)
												});
											}, seconds);
										});
									} else {
										$('.item-dialog').dialog('hide', function() {
											if (typeof resCSRF.url === 'string') {
												location.href = resCSRF.url;
												return false;
											}
											$('.item-dialog .item-edit-form .item-message').hide();
										});
									}
								});
			}
		});

		$('.item-dialog').dialog('show');
		$('.item-dialog').dialog('dismiss', function() {
			$('.item-dialog .item-edit-form').find('input[type=text],textarea')
				 							 .val('')
				 							 .validate('resolve');
			$('.item-dialog .item-edit-form #item-importance').adjustment('reset');
		});
	});

	$('.item-dialog .item-dialog-header .item-dialog-close').on('click touchstart', function(e) {
		$('.item-dialog').dialog('hide', function() {
			$('.item-dialog .item-edit-form').find('input[type=text],textarea')
				 							 .val('')
				 							 .validate('resolve');
			$('.item-dialog #item-deadline').datepicker('reset')
											.datepicker('hide');
			$('.item-dialog .item-edit-form #item-importance').adjustment('reset');
		});
	});

	$('.item-dialog .item-edit-form #item-importance').adjustment();
	$('.item-dialog .item-edit-form #item-deadline').datepicker();

	$('.list-item').each(function() {
		contents[$(this).attr('id')] = '.list-item-content[data-item-id=' + $(this).attr('id') + ']';
	}); 

	$('.list-tab').braziliantabs({
		contents: contents,
		onItemSelect: function(ul, content) {
			var status = parseInt(ul.data('item-status')),
				url = '/rest/item/get' + (!isNaN(status) ? '/' + status : '');
			
			if (category_id) url = (new UriParameters(url)).build({category: category_id}).getUrl();
			selectedItem = ul.attr('id');
			content.find('.list-item-count').hide();
			content.find('.list-single-item').each(function() {
				$(this).remove();
			});

			content.find('.list-item-edit').each(function() {
				$(this).remove();
			});

			getItemsAJAX.get(url)
						.success(function(res) {
							content.fadeIn(150);
							if (res.status == "OK") {
								if (!res.count) {
									if (typeof res.message === 'string') {
										content.find('.list-message')
											   .text(res.message)
											   .show();
									}
									ul.find('.item-count').text('');
								} else {
									var message = content.find('.list-message');
									message.text('');
									message.hide();
									content.find('.list-item-count').fadeIn(150, function() {
										var i = 0,
											tiktak = window.setInterval(function() {
												content.find('.list-item-count').text(i);
												if (i == res.count) {
													window.clearInterval(tiktak);
												}
												i++;
											}, 20);
									});

									if (status !== 2) {
										for (var i in res.data) {
											var item = $('<li class="list-single-item"></li>'),
												edit = $('<div class="list-item-edit"><i class="fa fa-pencil-square-o list-item-edit-icon"></i></div>'),
												del = $('<div class="list-item-delete"><i class="fa fa-trash list-item-delete-icon"></i></div>');
											item.append('<h5 class="list-item-title"></h5>');
											item.find('.list-item-title').text(res.data[i].item_title);
											item.data('item-id', res.data[i].item_id);
											item.appendTo(content.find('.list-item-wrapper'));
											edit.find('.list-item-edit-icon').css('display', 'block');
											edit.css({
												top: item.position().top + parseInt(item.css('margin-top')),
												height: item.outerHeight(false),
												display: 'block',
												padding: parseInt(item.css('padding')),
											}).insertBefore(item);
											edit.css({'left': item.position().left});

											del.find('.list-item-delete-icon').css('display', 'block');
											del.css({
												top: item.position().top + parseInt(item.css('margin-top')),
												height: item.outerHeight(false),
												display: 'block',
												padding: parseInt(item.css('padding')),
											}).insertAfter(item);
											del.css({'left': item.position().left + (item.width() * 0.97)});
											item.find('.list-item-title').css({'margin-left': edit.width() + 5});
										}
									} else {
										for (var i in res.data) {
											var item = $('<li class="list-single-item list-item-fiinish"></li>'),
												del = $('<div class="list-item-delete"><i class="fa fa-trash list-item-delete-icon"></i></div>');
											item.append('<h5 class="list-item-title"></h5>');
											item.find('.list-item-title').text(res.data[i].item_title);
											item.data('item-id', res.data[i].item_id);
											item.appendTo(content.find('.list-item-wrapper'));

											del.find('.list-item-delete-icon').css('display', 'block');
											del.css({
												top: item.position().top + parseInt(item.css('margin-top')),
												height: item.outerHeight(false),
												display: 'block',
												padding: parseInt(item.css('padding')),
											}).insertAfter(item);
											del.css({'left': item.position().left + (item.width() * 0.97)});
										}
									}

									if (ul.attr('id') != 'finish-item') {
										content.find('ul.list-item-wrapper').darg2sortable({
											onMove: function() {
												$('ul.list-item-wrapper').find('.list-item-edit').hide();
												$('ul.list-item-wrapper').find('.list-item-delete').hide();
											},
											onSorted: function() {
												var item_orders = {}, order = 1, data = {};
												$('ul.list-item-wrapper').find('.list-item-edit').remove();
												$('ul.list-item-wrapper').find('.list-item-delete').remove();

												$('.list-item-content ul.list-item-wrapper').find('li').each(function() {
													var item = $(this);
													item_orders[item.data('item-id')] = order;
													order++;
												});

												getItemsAJAX.get('/rest/csrf/refresh')
															.success(function(resCSRF) {
																getItemsAJAX.post('/rest/item/refresh/order', item_orders, resCSRF.csrf)
																    .success(function(res) {
																    	$('.list-item-content ul.list-item-wrapper').find('li').each(function() {
																			var item = $(this),
																				edit = $('<div class="list-item-edit"><i class="fa fa-pencil-square-o list-item-edit-icon"></i></div>'),
																				del = $('<div class="list-item-delete"><i class="fa fa-trash list-item-delete-icon"></i></div>');
																			edit.find('.list-item-edit-icon').css('display', 'block');
																			edit.css({
																				top: item.position().top + parseInt(item.css('margin-top')),
																				height: item.outerHeight(false),
																				display: 'block',
																				padding: parseInt(item.css('padding')),
																			}).insertBefore(item);
																			edit.css({'left': item.position().left});

																			del.find('.list-item-delete-icon').css('display', 'block');
																			del.css({
																				top: item.position().top + parseInt(item.css('margin-top')),
																				height: item.outerHeight(false),
																				display: 'block',
																				padding: parseInt(item.css('padding')),
																			}).insertAfter(item);
																			del.css({'left': item.position().left + (item.width() * 0.97)});
																			item.find('.list-item-title').css({'margin-left': edit.width() + 5});
																		});
																    });
															}).fail(function(resCSRF) {
																if (typeof resCSRF.url === 'string') {
																	location.href = resCSRF.url;
																} else {
																	location.href = '/login/show';
																}

																return false;
															});
													
											}
										});
									}
								}
							}
						})
						.fail(function(res) {
							if (typeof res.url === 'string') {
								location.href = res.url;
								return false;
							}

							if (typeof res.message === 'string') {
								content.find('.list-message')
									   .text(res.message)
									   .show();
							}

							content.css({
								'min-height': height - (height % 10),
								'display': '-webkit-flex',
								'display': 'flex',
								'-webkit-align-items': 'center',
								'align-items': 'center',
							});
						});
		}
	});

	getCategoryAJAX.get('/rest/category/get')
				   .success(function(res) {
						for (var i in res.data) {
							var category = $('<button class="btn btn-small btn-light category-item"><i class="fa fa-tag category-item-icon"></i><span class="category-item-title">' + res.data[i].category_name + '</span></button>');
							
							if (category_id) {											
								if (res.data[i].category_id == category_id) {
									category.removeClass('btn-light')
											.addClass('btn-info')
											.addClass('category-item-selected');
								}
							}

							category.data('category-id', res.data[i].category_id);
							category.appendTo('.category-bar');
						}
						
						if (!$('.category-bar .btn-info').length) {
							$('.category-bar .no-category').removeClass('btn-light').addClass('btn-info').addClass('category-item-selected');
						}
					});
});
