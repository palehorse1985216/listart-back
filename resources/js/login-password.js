var Ajax = require('mandeling-ajax/mandeling-ajax.js'),
	validate = require('burendo-validator/burendo-validator.jquery.js'),
	PwdAjax = new Ajax(),
	loginVue = new Vue({
		el: '#login',
		data: function() {
			return {};
		},
		methods: {
			submitForm: function(event) {
				event.preventDefault();
				$('.login-password-form').submit();
			}
		}
	});

$('.login-password-form').validate({
	format: {
		password: /^[A-Za-z0-9]{8,16}$/
	},
	message: {
		format: {
			password: '密碼須為8~16位英數組合'
		}
	},
	success: function() {
		var data = {}, duration = 280, seconds = 4000;
		data.password = $('input[name=password]').val();
		PwdAjax.get('/rest/csrf/refresh')
			   .success(function(resCSRF) {
					PwdAjax.post('/rest/authenticate/password', data, resCSRF.csrf)
						.success(function(res) {
							location.href = '/list/show';
						})
						.fail(function(res) {
							if (typeof res.message == 'string') {
								$('.response-message h6').text(res.message);
							} else {
								$('.response-message h6').text("密碼錯誤");
							}

							$('.response-message').animate({
								opacity: 1
							}, duration, function() {
								window.setTimeout(function() {
									$('.response-message').animate({
										opacity: 0
									}, duration, function() {
										$('input[name=password').val('');
									});
								}, seconds);
							});
						});
				})
				.fail(function(resCSRF) {
					$('.login-password-form .response-message h6').text('登入逾期');
					$('.login-password-form .response-message').animate({
						opacity: 1
					}, duration, function() {
						window.setTimeout(function() {
							$('.user-setting-form .response-message').animate({
								opacity: 0
							}, duration, function() {
								$('.login-password-form .response-message h6').text('');
								if (typeof resCSRF.url === 'string') {
									location.href = resCSRF.url;
								}
							});
						}, seconds);
					});
				});
	}
});