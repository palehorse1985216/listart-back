var Ajax = require('mandeling-ajax/mandeling-ajax.js'),
	validate = require('burendo-validator/burendo-validator.jquery.js'),
	loginAJAX = new Ajax(),
	loginVue = new Vue({
		el: '#login',
		data: function() {
			return {};
		},
		methods: {
			login: function(event, platform) {
				event.preventDefault();
				var url  = '/rest/login/' + platform;
				loginAJAX.get(url)
						 .success(function(res) {
							location.href = res.url;
							return false;
						 });
			},
			submitForm: function(event) {
				event.preventDefault();
				$('.login-form').submit();
			}
		}
	});

$('.login-form').validate({
	success: function() {
		var data = {}, duration = 250;
		data.email = $('.login-form').find('input[name=email]').val();
		loginAJAX.post('/rest/login/email', data)
				 .success(function(res) {
					var message = "";

					if (typeof res.url === 'string') {
						location.href = res.url;
						return false;
					}

					if (typeof res.message === 'string') {
						message = res.message;
					} else {
						message = "已發送驗證碼到您的電子信箱";
					}

					$('.login-message').text(message).animate({
						opacity: 1
					}, duration, function() {
						window.setTimeout(function() {
							$('.login-message').text(message).animate({
								opacity: 0
							}, duration * 2, function() {
								$('.login-form').find('input[name=email]').val('');
							});
						}, 4000);
					});
				}).fail(function(res) {
					$('.login-message').text(res.message).animate({
						opacity: 1
					}, duration);

					$('.login-form').find('input[name=email]').on('focus', function(e) {
						$('.login-message').text('').animate({
							opacity: 0
						}, duration);
					})
				});
	}
});