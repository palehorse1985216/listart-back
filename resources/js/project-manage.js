var dialog = require('baguette-dialog/baguette-dialog.jquery.js'),
	validate = require('burendo-validator/burendo-validator.jquery.js'),
	Ajax = require('mandeling-ajax/mandeling-ajax.js'),
	EmailAJAX = new Ajax(),
	DeleteMemberAJAX = new Ajax(),
	duration = 150, seconds = 1500;

$('body').delegate('.btn-add-member', 'click', function(e) {
	e.preventDefault();
	$('.email-dialog').dialog('show', function() {
		$('.email-dialog').dialog('dismiss', function() {
			$('.email-dialog .email-edit-form').validate('resolve');
		});
		$('.email-dialog .email-dialog-header .email-dialog-close').off('click').on('click', function(e) {
			e.preventDefault();
			$('.email-dialog').dialog('hide', function() {
				$('.email-dialog .email-edit-form').validate('resolve');
			});
		});
	});
});

$('body').delegate('.member-delete', 'click', function(e) {
	e.preventDefault();
	var Member = $(this);
	$('.confirm-dialog .confirm-dialog-content .confirm-dialog-content-text').text("確定讓" + $(this).attr('_name') + "離開？");
	$('.confirm-dialog .confirm-dialog-footer .confirm-dialog-negative').off('click').on('click', function(e) {
		$('.confirm-dialog').dialog('hide');
	});

	$('.confirm-dialog .confirm-dialog-footer .confirm-dialog-affirmative').off('click').on('click', function(e) {
		DeleteMemberAJAX.get('/rest/csrf/refresh')
						.success(function(resCSRF) {
							var data = {};
							data.member = Member.attr('_id');
							data.project = $('#project_id').val();
							DeleteMemberAJAX.post('/rest/member/delete', data, resCSRF.csrf)
											.success(function(res) {
												if (typeof res.message === 'string') {
													$('.message-dialog .message-dialog-content .message-dialog-content-text').text(res.message);
												} else {
													$('.message-dialog .message-dialog-content .message-dialog-content-text').text("已退出");
												}

												$('.confirm-dialog').dialog('hide', function() {
													$('.message-dialog').dialog('show', function() {
														window.setTimeout(function() {
															$('.message-dialog').dialog('hide', function() {
																$('.member-list .member-info[_id=' + data.member + ']').fadeOut(duration, function() {
																	$('.member-list .member-info[_id=' + data.member + ']').remove();
																});
															});
														}, seconds);
													});
												});
											}).fail(function(res) {
												if (typeof res.message === 'string') {
													$('.message-dialog .message-dialog-content .message-dialog-content-text').text(res.message);
												} else {
													$('.message-dialog .message-dialog-content .message-dialog-content-text').text("已退出");
												}
												
												$('.confirm-dialog').dialog('hide', function() {
													$('.message-dialog').dialog('show', function() {
														window.setTimeout(function() {
															$('.message-dialog').dialog('hide');
														}, seconds);
													});
												});
											});
						}).fail(function(resCSRF) {
							$('.confirm-dialog').dialog('hide', function() {
								if (typeof resCSRF.message === 'string') {
									$('.message-dialog .message-dialog-content .message-dialog-content-text').text(resCSRF.message);
									$('.message-dialog').dialog('show', function() {
										window.setTimeout(function() {
											$('.message-dialog').dialog('hide', function() {
												if (typeof resCSRF.url === 'string') {
													location.href = resCSRF.url;
													return false;
												}
											});
										}, seconds);
									});
								}
							});
						});
	});
	$('.confirm-dialog').dialog('show', function() {
		$('.confirm-dialog').dialog('dismiss', function() {
			$('.email-dialog .email-edit-form').validate('resolve');
		});
		$('.confirm-dialog .confirm-dialog-header .confirm-dialog-close').off('click').on('click', function(e) {
			e.preventDefault();
			$('.confirm-dialog').dialog('hide', function() {
				$('.email-dialog .email-edit-form').validate('resolve');
			});
		});
	});
});

$(document).ready(function() {
	$('.message-dialog .message-dialog-header .message-dialog-close').hide();
	$('.confirm-dialog').dialog();
	$('.message-dialog').dialog();
	$('.email-dialog').dialog();
	$('.email-dialog .email-edit-form').validate({
		success: function() {
			EmailAJAX.get('/rest/csrf/refresh')
					 .success(function(resCSRF) {
					 	var data = {};
					 	data.email = $('.email-dialog .email-edit-form #email').val();
					 	data.project = $('#project_id').val();
					 	EmailAJAX.post('/rest/invite/send', data, resCSRF.csrf)
					 			 .success(function(res) {
					 			 	if (typeof res.message === 'string') {
								 		$('.email-dialog .email-edit-form .email-message .response-message').text(res.message);
								 	} else {
								 		$('.email-dialog .email-edit-form .email-message .response-message').text("已發送邀請給" + data.email);
								 	}

								 	$('.email-dialog .email-edit-form .email-message').animate({opacity: 1}, duration, function() {
								 		window.setTimeout(function() {
								 			$('.email-dialog .email-edit-form .email-message').animate({opacity: 0}, duration, function() {
								 				$('.email-dialog').dialog('hide', function() {
								 					$('.email-dialog .email-edit-form').validate('resolve');
								 				});
								 			});
								 		}, seconds);
								 	});
					 			 }).fail(function(res) {
					 			 	if (typeof res.message === 'string') {
								 		$('.email-dialog .email-edit-form .email-message .response-message').text(res.message);
								 	} else {
								 		$('.email-dialog .email-edit-form .email-message .response-message').text("發送邀請失敗");
								 	}

								 	$('.email-dialog .email-edit-form .email-message').animate({opacity: 1}, duration, function() {
								 		window.setTimeout(function() {
								 			$('.email-dialog .email-edit-form .email-message').animate({opacity: 0}, duration);
								 		}, seconds);
								 	});
					 			 });
					 }).fail(function(resCSRF) {
					 	if (typeof resCSRF.message === 'string') {
					 		$('.email-dialog .email-edit-form .email-message .response-message').text(resCSRF.message);
					 	} else {
					 		$('.email-dialog .email-edit-form .email-message .response-message').text(登入逾期);
					 	}

					 	$('.email-dialog .email-edit-form .email-message').animate({opacity: 1}, duration, function() {
					 		window.setTimeout(function() {
					 			$('.email-dialog .email-edit-form .email-message').animate({opacity: 0}, duration, function() {
					 				if (typeof resCSRF.url === 'string') {
					 					$('.email-dialog').dialog('hide', function() {
					 						location.href = resCSRF.url;
					 						return false;
					 					});
					 				}
					 			});
					 		}, seconds);
					 	});
					 });
		}
	});
});