var validate = require('burendo-validator/burendo-validator.jquery.js'),
	Ajax = require('mandeling-ajax/mandeling-ajax.js'),
	PwdAJAX = new Ajax(),
	PasswordVue = new Vue({
		data: function() {
			return {};
		},
		methods: {
			UpdatePassword: function(e) {
				e.preventDefault();
				$('.user-password-form').submit();
			}
		}
	});

$('.user-password-form').validate({
	format: {
		password: /^[A-Za-z0-9]{8,16}$/
	},
	message: {
		format: {
			password: '密碼須為8~16位英數組合'
		}
	},
	success: function() {
		var data = {}, duration = 280, seconds = 3000;

		if ($('#user_password').val()) {
			data.password = $('#user_password').val();
		}

		data.new_password = $('#new_password').val();
		data.confirm_password = $('#confirm_password').val();
		
		if (data.new_password == data.confirm_password) {
			PwdAJAX.get('/rest/csrf/refresh')
				   .success(function(resCSRF) {
						PwdAJAX.post('/rest/user/password', data, resCSRF.csrf)
							   .success(function(res) {
									if (typeof res.message == 'string') {
										$('.response-message h6').text(res.message);
									} else {
										$('.response-message h6').text("修改成功");
									}

									$('.response-message').animate({
										opacity: 1
									}, duration, function() {
										window.setTimeout(function() {
											$('.response-message').animate({
												opacity: 0
											}, duration, function() {
												if (typeof res.url === 'string') {
													location.href = res.url;
												} else {
													location.href = '/list/show';
												}
											});
										}, seconds);
									});
								})
								.fail(function(res) {
									if (typeof res.message == 'string') {
										$('.response-message h6').text(res.message);
									} else {
										$('.response-message h6').text("修改失敗");
									}
									$('.response-message').animate({
										opacity: 1
									}, duration, function() {
										window.setTimeout(function() {
											$('.response-message').animate({
												opacity: 0
											}, duration);
										}, seconds);
									});
								});
					})
					.fail(function(resCSRF) {
						if (typeof resCSRF.message == 'string') {
							$('.response-message h6').text(resCSRF.message);
						} else {
							$('.response-message h6').text("修改失敗");
						}

						$('.response-message').animate({
							opacity: 1
						}, duration, function() {
							window.setTimeout(function() {
								$('.response-message').animate({
									opacity: 0
								}, duration, function() {
									if (typeof resCSRF.url === 'string') {
										location.href = resCSRF.url;
										return false;
									}
								});
							}, seconds);
						});
					});
			} else {
				$('.response-message h6').text("密碼不一致");
				$('.response-message').animate({
					opacity: 1
				}, duration, function() {
					window.setTimeout(function() {
						$('.response-message').animate({
							opacity: 0
						}, duration);
					}, seconds);
				});
			}
		
	}
});