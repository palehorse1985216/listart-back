var validate = require('burendo-validator/burendo-validator.jquery.js'),
	drag2upload = require('drag2upload/drag2upload.jquery.js'),
	Ajax = require('mandeling-ajax/mandeling-ajax.js'),
	avatarAJAX = new Ajax(),
	duration = 400, opacity=0.75, interval, timeout,
	locker = (function() {
		var lock = false;
		return {
			check: function() {
				return (lock) ? true : false;
			},
			set: function() {
				lock = true;
			},
			unset: function() {
				lock = false;
			}
		};
	})(),
	UserSettingVue = new Vue({
		el: "#user",
		data: function() {
			return {};
		},
		methods: {
			showUploadAvatarInstruction: function(e) {
				if (locker.check()) {
					return false;
				}
				$('.user-avatar-instruction .instruction-message').text("請將圖片拖曳至此");
				$('.user-avatar-instruction').animate({
					opacity: opacity
				}, duration);
			},
			hideUploadAvatarInstruction: function(e) {
				if (locker.check()) {
					return false;
				}
				$('.user-avatar-instruction .instruction-message').text("請將圖片拖曳至此");
				$('.user-avatar-instruction').animate({
					opacity: 0
				}, duration);
			},
			mobileAvatarUpload: function(e) {
				e.preventDefault();
				var file = $('input[type=file][name=avatar]'),
					form = new FormData(),
					xhr = new XMLHttpRequest();

				if (locker.check()) {
					return false;
				}
				locker.set();
				file.click();
				file.off().on('change', function() {
					var avatar = file.get(0).files[0];
					$('.user-avatar-instruction .instruction-message').text('上傳中');
					$('.user-avatar-instruction').animate({
						opacity: opacity
					}, duration, function() {
						interval = window.setInterval(function() {
							$('.user-avatar-instruction .instruction-message').fadeIn(duration, function() {
								$('.user-avatar-instruction .instruction-message').fadeOut(duration);
							})
						}, duration * 2);
					});

					avatarAJAX.get('/rest/csrf/refresh')
							  .success(function(resCSRF) {
									var data = {};
									data.avatar = avatar;
									avatarAJAX.upload('/rest/upload/avatar', data, resCSRF.csrf)
											  .success(function(res) {
													if (res.status == "OK") {
														window.clearInterval(interval);
														$('.user-avatar-instruction').animate({
															opacity: 0
														}, duration, function() {
															locker.unset();
															$('.user-avatar').css({
																backgroundImage: 'url("' + res.url + '")'
															});

															$('#user-select-menu').css({
																backgroundImage: 'url("' + res.url + '")'
															});
														});
													} else {
														$('.user-avatar-instruction .instruction-message').fadeOut(duration, function() {
															$('.user-avatar-instruction .instruction-message').fadeIn(duration, function() {
																if (typeof res.message === 'string') {
																	$('.user-avatar-instruction .instruction-message').text(res.message);
																} else {
																	$('.user-avatar-instruction .instruction-message').text('上傳失敗');
																}
															});
														});

														timeout = window.setTimeout(function() {
															window.clearInterval(interval);
															$('.user-avatar-instruction').animate({
																opacity: 0
															}, duration, function() {
																locker.unset();
															});
														}, 5000);
													}
												}).fail(function(res) {
													$('.user-avatar-instruction .instruction-message').fadeOut(duration, function() {
														$('.user-avatar-instruction .instruction-message').fadeIn(duration, function() {
															if (res.message && typeof res.message === 'string') {
																$('.user-avatar-instruction .instruction-message').text(res.message);
															} else {
																$('.user-avatar-instruction .instruction-message').text('上傳失敗');
															}
														});
													});
													
													window.setTimeout(function() {
														window.clearInterval(interval);
														timeout = window.setTimeout(function() {
															$('.user-avatar-instruction').animate({
																opacity: 0
															}, duration, function() {
																locker.unset();
															});
														}, 5000);
													}, duration);
												});

								})
								.fail(function(resCSRF) {
									window.clearInterval(interval);
									$('.user-avatar-instruction .instruction-message').fadeOut(duration, function() {
										$('.user-avatar-instruction .instruction-message').fadeIn(duration, function() {
											if (typeof resCSRF.message === 'string') {
												$('.user-avatar-instruction .instruction-message').text(resCSRF.message);
											} else {
												$('.user-avatar-instruction .instruction-message').text('上傳失敗');
											}
										});
									});

									window.setTimeout(function() {
										timeout = window.setTimeout(function() {
											$('.user-avatar-instruction').animate({
												opacity: 0
											}, duration, function() {
												locker.unset();
												if (typeof resCSRF.url === 'string') {
													location.href = resCSRF.url;
												}
											});
										}, 5000);
									}, duration);
								});
				});

			}
		}
	});

$(window).on('load', function() {
	$('.user-avatar-instruction').animate({
		opacity: 0
	}, 350);
});

$('.user-avatar').drag2upload({
	uploadFileUrl: '/rest/upload/avatar',
	onDragOver: function(e) {
		if (locker.check()) {
			return false;
		}
		locker.set();
		$('.user-avatar-instruction .instruction-message').text('準備上傳');
		$('.user-avatar-instruction').animate({
			opacity: opacity
		}, duration);
	},
	onDragOut: function(e) {
		$('.user-avatar-instruction .instruction-message').text('請將圖片拖曳至此');
		$('.user-avatar-instruction').animate({
			opacity: 0
		}, duration, function() {
			locker.unset();
		});
	},
	onDrop: function(e) {
		if (typeof timeout !== 'undefined') {
			window.clearTimeout(timeout);
		}
		$('.user-avatar-instruction .instruction-message').text('上傳中');
		interval = window.setInterval(function() {
			$('.user-avatar-instruction .instruction-message').fadeOut(duration, function() {
				$('.user-avatar-instruction .instruction-message').fadeIn(duration);
			});
		}, duration * 2);
	},
	onUploaded: function(res) {
		if (res.status == "OK") {
			window.clearInterval(interval);
			$('.user-avatar-instruction').animate({
				opacity: 0
			}, duration, function() {
				locker.unset();
				$('.user-avatar').css({
					backgroundImage: 'url("' + res.url + '")'
				});

				$('#user-select-menu').css({
					backgroundImage: 'url("' + res.url + '")'
				});
			});
		} else {
			$('.user-avatar-instruction .instruction-message').fadeOut(duration, function() {
				$('.user-avatar-instruction .instruction-message').fadeIn(duration, function() {
					if (typeof res.message === 'string') {
						$('.user-avatar-instruction .instruction-message').text(res.message);
					} else {
						$('.user-avatar-instruction .instruction-message').text('上傳失敗');
					}
				});
			});

			window.setTimeout(function() {
				window.clearInterval(interval);
				timeout = window.setTimeout(function() {
					$('.user-avatar-instruction').animate({
						opacity: 0
					}, duration, function() {
						locker.unset();
					});
				}, 5000);
			}, duration);
		}
	},
	onError: function(res) {
		$('.user-avatar-instruction .instruction-message').fadeOut(duration, function() {
			$('.user-avatar-instruction .instruction-message').fadeIn(duration, function() {
				if (typeof res.message === 'string') {
					$('.user-avatar-instruction .instruction-message').text(res.message);
				} else {
					$('.user-avatar-instruction .instruction-message').text('上傳失敗');
				}
			});
		});

		window.setTimeout(function() {
			window.clearInterval(interval);
			timeout = window.setTimeout(function() {
				$('.user-avatar-instruction').animate({
					opacity: 0
				}, duration, function() {
					locker.unset();
				});
			}, 5000);
		}, duration);
	},
	onRefreshCSRFFail: function(res) {
		$('.user-avatar-instruction .instruction-message').fadeOut(duration, function() {
			$('.user-avatar-instruction .instruction-message').fadeIn(duration, function() {
				if (typeof res.message === 'string') {
					$('.user-avatar-instruction .instruction-message').text(res.message);
				} else {
					$('.user-avatar-instruction .instruction-message').text('上傳失敗');
				}
			});
		});

		window.setTimeout(function() {
			window.clearInterval(interval);
			timeout = window.setTimeout(function() {
				$('.user-avatar-instruction').animate({
					opacity: 0
				}, duration, function() {
					locker.unset();
					if (typeof res.url === 'string') {
						location.href = res.url;
					}
				});
			}, 5000);
		}, duration);
	},
 	refreshCsrfUrl: '/rest/csrf/refresh',
	csrf: $('meta[name="csrf-token"]').attr('content'),
	name: 'avatar'
});

$('.user-setting-form').validate({
	success: function() {
		var duration = 350,
			seconds = 2300;
			ajax.get('/rest/csrf/refresh')
				.success(function(res) {
					var data = {};
					if ($('.user-setting-form #user_name').length) {
						data.name = $('.user-setting-form #user_name').val();
					}
					data.nickname = $('.user-setting-form #user_nickname').val();
					ajax.post('/rest/user/update', data, res.csrf)
						.success(function(res) {
							$('.user-setting-form .response-message h6').text('修改成功');
							$('.user-setting-form .response-message').animate({
								opacity: 1
							}, duration, function() {
								window.setTimeout(function() {
									$('.user-setting-form .response-message').animate({
										opacity: 0
									}, duration, function() {
										$('.user-setting-form .response-message h6').text('');
									});
								}, seconds);
							});
						})
						.fail(function(res) {
							if (res.message) {
								$('.user-setting-form .response-message h6').text(res.message);
							} else {
								$('.user-setting-form .response-message h6').text('修改失敗');
							}
							
							$('.user-setting-form .response-message').animate({
								opacity: 1
							}, duration, function() {
								window.setTimeout(function() {
									$('.user-setting-form .response-message').animate({
										opacity: 0
									}, duration, function() {
										$('.user-setting-form .response-message h6').text('');
										if (typeof res.url === 'string') {
											location.href = res.url;
										}
									});
								}, seconds);
							});
						});
				})
				.fail(function(res) {
					$('.user-setting-form .response-message h6').text('登入逾期');
					$('.user-setting-form .response-message').animate({
						opacity: 1
					}, duration, function() {
						window.setTimeout(function() {
							$('.user-setting-form .response-message').animate({
								opacity: 0
							}, duration, function() {
								$('.user-setting-form .response-message h6').text('');
								if (typeof res.url === 'string') {
									location.href = res.url;
								}
							});
						}, seconds);
					});
				});
	}
});