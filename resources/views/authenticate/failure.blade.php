@extends('layouts/authenticate')

@section('content')
	<div class="authenticate-box">
		<div class="form-group">
			<h2 class="page-title">{{$title}}</h2>
		</div>
		<div class="form-group">
			<h4 class="authenticate-message">{{$message}}</h4>
		</div>
		<div class="form-group">
			<a class="btn btn-secondary btn-failure-redirect" href="/login/show">返回</a>
		</div>
	</div>
@endsection