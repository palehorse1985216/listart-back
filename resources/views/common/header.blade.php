<div id="page-header" class="page-header">
	<div class="header-container container">
	@if (!empty(session('user')))
		<div id="user-select-menu" class="header-avatar header-item select-down-toggle">
			<div class="header-item-cover"></div>
		</div>
		<ul class="select-menu">
			<li><span class="user-name">{{!empty(session('user')['user_name']) ? session('user')['user_name'] : '無名氏'}}</span></li>
			<li><a href="{{route('user-setting')}}">設定資料</a></li>
			@if (session('user')['user_login_type'] == 4)
			<li><a href="{{route('user-password')}}">設定密碼</a></li>
			@endif
			<li><a href="{{route('item-show')}}">分類</a></li>
			<li><a href="{{route('project-show')}}">專案</a></li>
			<li><a href="" v-on:click="logout">登出</a></li>
		</ul>
	@endif
		<a href="/" class="header-item header-item-home" style="background-image: url('/images/home.png')"></a>
	</div>
</div>