<title>{{env('APP_NAME')}}</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="csrf-token" content="{{ csrf_token() }}">
<!-- GSuit -->
<meta name="google-site-verification" content="AZZPyNU_H1bLkJiimXALmqrglzkhIFMO6lAbOTECijk" />
<!-- GSuit -->