@component('mail::message')

#### 歡迎您使用電子郵件驗證，請點擊下方連結，驗證您的電子郵件，以取得帳號

@component('mail::button', ['url' => $url])
{{$button}}
@endcomponent

{{ env('APP_NAME') }}
@endcomponent
