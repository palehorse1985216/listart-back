@component('mail::message')

#### {{$admin}}邀請您加入『{{$project}}』專案

@component('mail::button', ['url' => $accept_url])
{{$accept}}
@endcomponent

@component('mail::button', ['url' => $reject_url])
{{$reject}}
@endcomponent

{{ env('APP_NAME') }}
@endcomponent