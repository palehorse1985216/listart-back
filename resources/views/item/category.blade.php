<div class="category-bar">
	<button class="btn btn-small btn-success btn-add-new-category"><i class="fa fa-plus category-item-icon"></i><span class="category-item-title">分類</span></button>
	<a href="/item/show" class="btn btn-small btn-light no-category"><span class="category-item-title">無分類</span></a>
</div>