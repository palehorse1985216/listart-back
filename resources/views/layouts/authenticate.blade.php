<!doctype html>
<html>
	<head>
		@include('common.meta')
		<link defer href="{{secure_asset('css/authenticate.css')}}?{{time()}}" rel="stylesheet">
	</head>
	<body>
		<div class="authenticate-container-wrapper">
		@include('common.header')
			<div class="authenticate-container container" id="authenticate">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">
						@yield('content')
					    @include('common.copyright')
                                        </div>
					<div class="col-md-3"></div>
				</div>
			</div>
                        
		</div>
	</body>
</html>
