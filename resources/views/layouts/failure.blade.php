<!doctype html>
<html>
	<head>
		@include('common.meta')
		<link defer href="{{secure_asset('css/failure.css')}}?{{time()}}" rel="stylesheet">
		<script type="text/javascript" src="{{secure_asset('js/header.js')}}?{{time()}}"></script>
	</head>
	<body>
		<div class="failure-container-wrapper">
		@include('common.header')
			<div class="failure-container container" id="failure">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">
						@yield('content')
					    @include('common.copyright')
                                        </div>
					<div class="col-md-3"></div>
				</div>
			</div>
                        
		</div>
	</body>
</html>
