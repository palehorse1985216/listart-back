<!doctype html>
<html>
	<head>
		@include('common.meta')
		<link defer href="{{secure_asset('css/item.css')}}?{{time()}}" rel="stylesheet">
		<script type="text/javascript" src="{{secure_asset('js/item.js')}}?{{time()}}"></script>
	</head>
	<div class="top-message-wrapper">
		<div class="top-message-container">
			<h6 class="message-content"></h6>
		</div>
	</div>
	<body>
		<div class="item-container-wrapper">
		@include('common.header')
			<div class="item-container container" id="item">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">@include('item.category')</div>
					<div class="col-md-2"></div>
				</div>
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						@yield('content')
					    @include('common.copyright')
                                        </div>
					<div class="col-md-2"></div>
				</div>
			</div>
                        
		</div>
	</body>
	<div class="item-edit-tool">
		<i class="fa fa-pencil-square-o"></i>
	</div>
	<div class="category-dialog">
		<div class="category-dialog-header"><i class="fa fa-times fa-lg category-dialog-close"></i></div>
		<form class="category-edit-form">
			<div class="form-group">
				<label for="category-name">分類名稱</label>
				<input id="category-name" type="text" name="name" class="form-control" placeholder="建立新分類" autocomplete="off" data-required-field />
			</div>
			<input id="category-id" type="hidden" name="category_id" />
			<div class="form-group">
				<button class="btn btn-primary btn-submit btn-category-update">儲存</button>
			</div>
			<div class="form-group">
				<button class="btn btn-danger btn-submit btn-category-delete"><i class="fa fa-trash btn-category-delete-icon"></i>刪除</button>
			</div>
			<div class="form-group">
				<div class="category-message">
					<h6 class="response-message"></h6>
				</div>
			</div>
		</form>
	</div>
	<div class="item-dialog">
		<div class="item-dialog-header"><i class="fa fa-times fa-lg item-dialog-close"></i></div>
		<form class="item-edit-form">
			<div class="form-group">
				<label for="item-title">標題</label>
				<input id="item-title" type="text" name="title" class="form-control" autocomplete="off" data-required-field />
			</div>
			<div class="form-group">
				<label for="item-deadline">截止日期</label>
				<input id="item-deadline" type="text" name="deadline" class="form-control" autocomplete="off" data-validate-type="date">
			</div>
			<div class="form-group">
				<label for="item-content">敘述</label>
				<textarea id="item-content" type="text" name="content" class="form-control" placeholder="寫點東西......"></textarea>
			</div>
			<div class="form-group">
				<label for="item-importance">重要度</label>
				<input id="item-importance" type="number" name="importance" max=10 class="form-control">
			</div>
			<input id="item-id" type="hidden" name="item_id" />
			<div class="form-group">
				<button class="btn btn-primary btn-submit btn-item-update">儲存</button>
			</div>
			<div class="form-group">
				<button class="btn btn-success btn-item-process">開始進行<i class="fa fa-arrow-circle-right"></i></button>
				<button class="btn btn-success btn-item-complete">已完成<i class="fa fa-check"></i></button>
			</div>
			<div class="form-group">
				<div class="item-message">
					<h6 class="response-message"></h6>
				</div>
			</div>
		</form>
	</div>
	<div class="confirm-dialog">
		<div class="confirm-dialog-header"><i class="fa fa-times fa-lg confirm-dialog-close"></i></div>
		<div class="confirm-dialog-content">
			<h4 class="confirm-dialog-content-text"></h4>
		</div>
		<div class="confirm-dialog-footer">
			<button class="btn btn-secondary confirm-dialog-affirmative">是</button>
			<button class="btn btn-secondary confirm-dialog-negative">否</button>
		</div>
		<div class="confirm-message-wrapper">
			<div class="confirm-message">
				<h6 class="response-message"></h6>
			</div>
		</div>
	</div>
	<div class="message-dialog">
		<div class="message-dialog-header"><i class="fa fa-times fa-lg message-dialog-close"></i></div>
		<div class="message-dialog-content">
			<h4 class="message-dialog-content-text"></h4>
		</div>
	</div>
</html>
