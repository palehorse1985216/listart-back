<!doctype html>
<html>
	<head>
		@include('common.meta')
		<link defer href="{{secure_asset('css/'.$css.'.css')}}?{{time()}}" rel="stylesheet">
	</head>
	<body>
		<div class="login-container-wrapper">
		@include('common.login-header')
			<div class="login-container container" id="login">
				<div class="row">
					<div class="col-md-3"></div>
					<div class="col-md-6">
						@yield('content')
					    @include('common.copyright')
                                        </div>
					<div class="col-md-3"></div>
				</div>
			</div>
                        
		</div>
	</body>
	<script type="text/javascript" src="{{secure_asset('js/'. $js . '.js')}}?{{time()}}"></script>
</html>
