<!doctype html>
<html>
	<head>
		@include('common.meta')
		<link defer href="{{secure_asset('css/'.$css.'.css')}}?{{time()}}" rel="stylesheet">
	</head>
	<body>
		<div class="manage-container-wrapper">
		@include('common.header')
			<div class="manage-container container" id="manage">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						@yield('content')
					    @include('common.copyright')
                    </div>
					<div class="col-md-2"></div>
				</div>
			</div>
                        
		</div>
	</body>
	<div class="message-dialog">
		<div class="message-dialog-header"><i class="fa fa-times fa-lg message-dialog-close"></i></div>
		<div class="message-dialog-content">
			<h4 class="message-dialog-content-text"></h4>
		</div>
	</div>
	<div class="email-dialog">
		<div class="email-dialog-header"><i class="fa fa-times fa-lg email-dialog-close"></i></div>
		<form class="email-edit-form">
			<div class="form-group">
				<h4 class="email-title"></h4>
			</div>
			<div class="form-group">
				<label for="email">寄送邀請給對方</label>
				<input id="email" type="email" name="email" class="form-control" placeholder="XXX@example.com" autocomplete="off" data-required-field />
			</div>
			<div class="form-group">
				<button class="btn btn-primary btn-submit btn-email-send">發送</button>
			</div>
			<div class="email-message-wrapper">
				<div class="email-message">
					<h6 class="response-message"></h6>
				</div>
			</div>
		</form>
	</div>
	<div class="confirm-dialog">
		<div class="confirm-dialog-header"><i class="fa fa-times fa-lg confirm-dialog-close"></i></div>
		<div class="confirm-dialog-content">
			<h4 class="confirm-dialog-content-text"></h4>
		</div>
		<div class="confirm-dialog-footer">
			<button class="btn btn-secondary confirm-dialog-affirmative">是</button>
			<button class="btn btn-secondary confirm-dialog-negative">否</button>
		</div>
		<div class="confirm-message-wrapper">
			<div class="confirm-message">
				<h6 class="response-message"></h6>
			</div>
		</div>
	</div>
	<script type="text/javascript" src="{{secure_asset('js/'. $js . '.js')}}?{{time()}}"></script>
</html>
