<!doctype html>
<html>
	<head>
		@include('common.meta')
		<link defer href="{{secure_asset('css/'.$css.'.css')}}?{{time()}}" rel="stylesheet">
		<script type="text/javascript" src="{{secure_asset('js/'.$js.'.js')}}?{{time()}}"></script>
	</head>
	<body>
		<div class="privacy-container-wrapper">
		@include('common.header')
			<div class="privacy-container container" id="privacy">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						@yield('content')
					    @include('common.copyright')
                    </div>
					<div class="col-md-2"></div>
				</div>
			</div> 
		</div>
	</body>
</html>
