<!doctype html>
<html>
	<head>
		@include('common.meta')
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<link defer href="{{secure_asset('css/project.css')}}?{{time()}}" rel="stylesheet">
		<script type="text/javascript" src="{{secure_asset('js/project.js')}}?{{time()}}"></script>
	</head>
	<div class="top-message-wrapper">
		<div class="top-message-container">
			<h6 class="message-content"></h6>
		</div>
	</div>
	<body>
		<div class="item-container-wrapper">
		@include('common.header')
			<div class="item-container container" id="item">
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">@include('project.project')</div>
					<div class="col-md-2"></div>
				</div>
				<div class="row">
					<div class="col-md-2"></div>
					<div class="col-md-8">
						@yield('content')
					    @include('common.copyright')
                                        </div>
					<div class="col-md-2"></div>
				</div>
			</div>
                        
		</div>
	</body>
	<div class="item-edit-tool">
		<i class="fa fa-pencil-square-o"></i>
	</div>
	<div class="project-dialog">
		<div class="project-dialog-header"><i class="fa fa-times fa-lg project-dialog-close"></i></div>
		<form class="project-edit-form">
			<div class="form-group">
				<label for="project-name">專案名稱</label>
				<input id="project-name" type="text" name="name" class="form-control" placeholder="建立一個新專案" autocomplete="off" data-required-field />
			</div>
			<input id="project-id" type="hidden" name="project_id" />
			<div class="form-group">
				<button class="btn btn-primary btn-submit btn-project-update">儲存</button>
			</div>
			<div class="form-group">
				<a href="/project/manage/" class="btn btn-secondary btn-submit btn-project-manage"><i class="fa fa-cog btn-project-manage-icon"></i>管理</a>
			</div>
			<div class="form-group">
				<button class="btn btn-danger btn-submit btn-project-delete"><i class="fa fa-trash btn-project-delete-icon"></i>刪除</button>
			</div>
			<div class="form-group">
				<div class="project-message">
					<h6 class="response-message"></h6>
				</div>
			</div>
		</form>
	</div>
	<div class="item-dialog">
		<div class="item-dialog-header"><i class="fa fa-times fa-lg item-dialog-close"></i></div>
		<form class="item-edit-form">
			<div class="form-group">
				<label for="item-title">標題</label>
				<input id="item-title" type="text" name="title" class="form-control" autocomplete="off" data-required-field />
			</div>
			<div class="form-group">
				<label for="item-deadline">截止日期</label>
				<input id="item-deadline" type="text" name="deadline" class="form-control" autocomplete="off" data-validate-type="date">
			</div>
			<div class="form-group">
				<label for="item-content">敘述</label>
				<textarea id="item-content" type="text" name="content" class="form-control" placeholder="寫點東西......"  data-required-field></textarea>
			</div>
			<div class="form-group">
				<label for="item-importance">重要度</label>
				<input id="item-importance" type="number" name="importance" max=10 class="form-control">
			</div>
			<input id="item-id" type="hidden" name="item_id" />
			<div class="form-group">
				<button class="btn btn-primary btn-submit btn-item-update">儲存</button>
			</div>
			<div class="form-group">
				<button class="btn btn-success btn-item-process">開始進行<i class="fa fa-arrow-circle-right"></i></button>
				<button class="btn btn-success btn-item-complete">已完成<i class="fa fa-check"></i></button>
			</div>
			<div class="form-group">
				<div class="item-message">
					<h6 class="response-message"></h6>
				</div>
			</div>
		</form>
	</div>
	<div class="confirm-dialog">
		<div class="confirm-dialog-header"><i class="fa fa-times fa-lg confirm-dialog-close"></i></div>
		<div class="confirm-dialog-content">
			<h4 class="confirm-dialog-content-text"></h4>
		</div>
		<div class="confirm-dialog-footer">
			<button class="btn btn-secondary confirm-dialog-affirmative">是</button>
			<button class="btn btn-secondary confirm-dialog-negative">否</button>
		</div>
		<div class="confirm-message-wrapper">
			<div class="confirm-message">
				<h6 class="response-message"></h6>
			</div>
		</div>
	</div>
	<div class="message-dialog">
		<div class="message-dialog-header"><i class="fa fa-times fa-lg message-dialog-close"></i></div>
		<div class="message-dialog-content">
			<h4 class="message-dialog-content-text"></h4>
		</div>
	</div>
</html>
