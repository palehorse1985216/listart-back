@extends('layouts/login')

@section('content')
	<form class="login-form">
		<div class="form-group">
			<h2 class="page-title">{{$title}}</h2>
		</div>
		<div class="form-group">
			<button class="btn btn-light btn-facebook-login" v-on:click="login(event, 'facebook')">
				<img class="login-icon" src="{{secure_asset('/images/facebook.png')}}" />Facebook 登入
			</button>
		</div>
		<div class="form-group">
			<button class="btn btn-light btn-google-login" v-on:click="login(event, 'google')">
				<img class="login-icon" src="/images/search.png" />Google 登入
			</button>
		</div>
		<div class="form-group">
			<button class="btn btn-light btn-twitter-login" v-on:click="login(event, 'twitter')">
				<img class="login-icon" src="/images/twitter.png" />Twitter 登入
			</button>
		</div>
		<!--<div class="form-group">
			<button class="btn btn-light btn-instagram-login" v-on:click="login(event, 'instagram')">
				<img class="login-icon" src="/images/instagram.png" />Instagram 登入
			</button>
		</div>-->
		<div class="form-group">
			<div class="input-group">
				<div class="input-group-prepend">
					<span class="input-group-text"><img class="login-icon" src="/images/email.png" /></span>
				</div>
				<input type="email" name="email" class="input-email-login form-control" placeholder="Email登入" v-on:keydown.enter="submitForm" data-required-field>
				<div class="input-group-append">
					<button class="btn btn-outline-secondary" type="button" v-on:click="submitForm">Go</button>
				</div>
			</div>
			<span class="login-message"></span>
		</div>
	</form>
@endsection