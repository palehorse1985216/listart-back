@extends('layouts/login')

@section('content')
	<form class="login-password-form">
		<div class="form-group">
			<h2 class="page-title">{{$title}}</h2>
		</div>
		<div class="form-group">
			<div class="input-group">
				<input type="password" name="password" class="input-password-login form-control" placeholder="請輸入密碼" v-on:keydown.enter="submitForm" required>
			</div>
		</div>
		<div class="form-group">
			<button class="btn btn-secondary btn-password-login">登入</button>
		</div>
		<div class="form-group">
			<div class="form-control response-message"><h6></h6></div>
		</div>
	</form>
@endsection