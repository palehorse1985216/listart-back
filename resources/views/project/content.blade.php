@extends('layouts/project')

@section('content')
	<form class="item-form">
		<ul class="list-tab">
		  <li id="progress-item" class="list-item" data-item-status="1">
		    <a href="">進行中</a>
		  </li>
		  <li id="todo-item" class="list-item" data-item-status="0">
		    <a href="">待辦事項</a>
		  </li>
		  <li id="finish-item" class="list-item" data-item-status="2">
		    <a href="">已完成</a>
		  </li>
		  <button class="btn btn-success btn-sm add-new-item"><i class="fa fa-plus"></i></button>
		</ul>
		<button class="btn btn-success mobile-add-new-item"><i class="fa fa-plus"></i></button>
		<!-- content of Progress -->
		<div class="list-item-content" data-item-id="progress-item">
			<h4 class="list-message"></h4>
			<span class="list-item-count">0</span>
			<ul class="list-item-wrapper"></ul>
		</div>

		<!-- content of Todo -->
		<div class="list-item-content" data-item-id="todo-item">
			<h4 class="list-message"></h4>
			<span class="list-item-count">0</span>
			<ul class="list-item-wrapper"></ul>
		</div>

		<!-- content of Finish -->
		<div class="list-item-content" data-item-id="finish-item">
			<h4 class="list-message"></h4>
			<span class="list-item-count">0</span>
			<ul class="list-item-wrapper"></ul>
		</div>
	</form>
@endsection