@extends('layouts/failure')

@section('content')
	<div class="failure-box">
		<div class="form-group">
			<h2 class="page-title">{{$title}}</h2>
		</div>
		<div class="form-group">
			<h4 class="failure-message">{{$message}}</h4>
		</div>
		<div class="form-group">
			<a class="btn btn-secondary btn-failure-redirect" href="/">返回</a>
		</div>
	</div>
@endsection