@extends('layouts/manage')

@section('content') 
	<form class="project-manage-form" novalidate>
		<input id="project_id" type="hidden" name="project_id" value="{{$project_id}}" />
		<div class="form-group">
			<h1 class="page-title">{{$name}}</h1>
		</div>
		<div class="form-group">
			<h6 class="member-list-title">專案成員</h6>
			<button class="btn btn-success btn-add-member">
				<i class="fa fa-plus btn-add-member-icon"></i>
				成員
			</button>
		</div>
		<div class="form-group member-list-wrapper">
			<ul class="member-list" id="member_list">
				@foreach ($Members as $Member)
				<li class="member-info" _id="{{$Member->member_id}}">
					<h6 class="member-role">{{$Member->role}}</h6>
					<h6 class="member-name">{{$Member->name}} <span class="member-email">({{$Member->email}})</span>
						@if (session('user')['user_id'] != $Member->user_id && $role == 'admin')
						<i class="fa fa-lg fa-times member-delete" _id="{{$Member->member_id}}" _name="{{$Member->name}}"></i>
						@endif
					</h6>
				</li>
				@endforeach
			</ul>
		</div>
	</form>
@endsection