@extends('layouts/user')

@section('content') 
	<form class="user-password-form" novalidate>
		<div class="form-group">
			<h1 class="page-title">{{$title}}</h1>
		</div>
		@if ($user_password)
		<div class="form-group">
			<label for="user_password">原密碼</label>
			<input id="user_password" type="password" name="user_password" class="form-control" autocomplete=false required />
		</div>
		@endif
		<div class="form-group">
			<label for="new_password">新密碼</label>
			<input id="new_password" type="password" name="new_password" class="form-control" autocomplete=false required on:keydown="UpdatePassword"/>
		</div>
		<div class="form-group">
			<label for="confirm_password">確認密碼</label>
			<input id="confirm_password" type="password" name="confirm_password" class="form-control" autocomplete=false required on:keydown="UpdatePassword"/>
		</div>
		<div class="form-group">
			<button class="btn btn-secondary btn-save-user" on:click="UpdatePassword">送出</button>
			<a class="btn btn-secondary btn-cancel-user" href="/">取消</a>
		</div>
		<div class="form-group">
			<div class="form-control response-message"><h6></h6></div>
		</div>
	</form>
@endsection
