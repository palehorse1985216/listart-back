@extends('layouts/user')

@section('content') 
	<form class="user-setting-form" novalidate>
		<div class="form-group">
			<h1 class="page-title">{{$title}}</h1>
		</div>
		<div class="form-group">
			<div class="avatar-wrapper">
				<div class="user-avatar" @if(!empty($avatar)) style="background-image: url('{{$avatar}}');"@endif @mouseenter="showUploadAvatarInstruction" @mouseleave="hideUploadAvatarInstruction">
						<div class="user-avatar-instruction" @if(empty($avatar)) style="display: none;" @endif >
							<h5 class="instruction-message">
							</h5>
						</div>
						@if(empty($avatar)) 
						<h4 class="no-avatar">尚無頭像</h4>
						@endif
				</div>
				<button class="btn btn-secondary btn-upload-avatar" @click="mobileAvatarUpload">
					<i class="fas fa-cloud-upload-alt"></i>
					上傳頭像
				</button>
				<a href="/download/avatar" class="btn btn-secondary btn-download-avatar">
					<i class="fas fa-cloud-download-alt"></i>
					下載頭像
				</a>
			</div>
		</div>
		<div class="form-group">
			<label for="user_email">電子信箱</label>
			<h5 class="user-email">{{$email}}</h5>
		</div>
		<div class="form-group">
			<label for="user_name">姓名</label>
			@if (session('user')['user_login_type'] != 4)
			<h5 class="user-name">{{$name}}</h5>
			@else
			<input id="user_name" type="text" name="user_name" class="form-control" value="{{!empty($name) ? $name : ''}}" />
			@endif
		</div>
		<div class="form-group">
			<label for="user_nickname">暱稱</label>
			<input id="user_nickname" type="text" name="user_nickname" class="form-control" value="{{$nickname}}" />
			<span class="user-message hidden"></span>
		</div>
		<div class="form-group">
			<button class="btn btn-secondary btn-save-user">儲存</button>
		</div>
		<div class="form-group">
			<div class="form-control response-message"><h6></h6></div>
		</div>
	</form>
	<input type="file" name="avatar"/>
@endsection
