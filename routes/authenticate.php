<?php

use Illuminate\Http\Request;

Route::prefix('authenticate')->group(function() {
	Route::get('/facebook', 'AuthenticateController@facebook')->name('authenticate-facebook');
	Route::get('/google', 'AuthenticateController@google')->name('authenticate-google');
	Route::get('/twitter', 'AuthenticateController@twitter')->name('authenticate-twitter');
	Route::get('/email/{code}', 'AuthenticateController@email')->name('authenticate-email');

	//驗證失敗頁面
	Route::get('/failure/{type}/{code}', 'AuthenticateController@failure')->name('authenticate-failure');
});