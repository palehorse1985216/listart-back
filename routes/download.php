<?php

use Illuminate\Http\Request;

Route::prefix('download')->group(function() {
	Route::get('/avatar', 'DownloadController@avatar')->name('download-avatar')->middleware('user');
});