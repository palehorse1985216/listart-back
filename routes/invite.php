<?php

use Illuminate\Http\Request;

Route::prefix('invite')->group(function() {
	Route::get('/accept/{user_id}/{project_id}', 'InviteController@acceptInvitation')
		 ->name('invite-accept')
		 ->middleware('user');

	Route::get('/reject/{user_id}/{project_id}', 'InviteController@rejectInvitation')
		 ->name('invite-reject')
		 ->middleware('user');
});