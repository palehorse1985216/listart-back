<?php

use Illuminate\Http\Request;

Route::prefix('item')->group(function() {
	Route::get('/show', 'ItemController@show')->name('item-show')->middleware('user');
});