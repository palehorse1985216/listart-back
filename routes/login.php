<?php

use Illuminate\Http\Request;

Route::prefix('login')->group(function() {
	Route::get('/show', 'LoginController@show')->name('login-show');
	Route::get('/password', 'LoginController@password')->name('login-password');
});