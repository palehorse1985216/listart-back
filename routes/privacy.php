<?php

use Illuminate\Http\Request;

Route::prefix('privacy')->group(function() {
	Route::get('/policy', 'PrivacyController@policy')->name('privacy-policy');
});