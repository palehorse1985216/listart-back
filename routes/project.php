<?php

use Illuminate\Http\Request;

Route::prefix('project')->group(function() {
	Route::get('/show', 'ProjectController@show')
		 ->name('project-show')
	     ->middleware('user')
	     ->middleware('check-project-member');

	Route::get('/manage', 'ProjectController@manage')
		 ->name('project-manage')
	     ->middleware('user')
	     ->middleware('check-project-member')
	     ->middleware('check-project-role');

	Route::get('/failure/{code}', 'ProjectController@failure')
		 ->name('project-failure');
});