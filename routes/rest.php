<?php

use Illuminate\Http\Request;

Route::prefix('rest')->group(function() {
	//登入
	Route::get('/login/facebook', 'RestLoginController@facebook')->name('rest-login-facebook');
	Route::get('/login/google', 'RestLoginController@google')->name('rest-login-google');
	Route::get('/login/twitter', 'RestLoginController@twitter')->name('rest-login-twitter');
	Route::post('/login/email', 'RestLoginController@email')
		 ->name('rest-login-email')
		 ->middleware('email-verify')
		 ->middleware('email-exist');

	Route::post('/authenticate/password', 'RestAuthenticateController@password')
		 ->name('rest-authenticate-password')
		 ->middleware('password-verify');

	//登出		
	Route::get('/user/logout', 'RestUserController@logout')->name('rest-user-logout');

	//取得頭像
	Route::get('/user/avatar', 'RestUserController@avatar')->name('rest-user-avatar')->middleware('user');

	//上傳頭像
	Route::post('/upload/avatar', 'RestUploadController@avatar')->name('rest-upload-avatar')->middleware('user', 'avatar');

	//修改會員基本資料
	Route::post('/user/update', 'RestUserController@update')
		 ->name('rest-user-update')
		 ->middleware('user');

	//修改會員密碼
	Route::post('/user/password', 'RestUserController@password')
		 ->name('rest-user-password')
		 ->middleware('user')
		 ->middleware('password-check');

	//取得使用者的所有分類
	Route::get('/category/get', 'RestCategoryController@getCategory')
		 ->name('rest-category-get')
		 ->middleware('user');

	//取得使用者的單一分類
	Route::get('/category/fetch/{category_id}', 'RestCategoryController@fetchCategory')
		 ->name('rest-category-fetch')
		 ->middleware('user');

	//建立使用者的分類
	Route::post('/category/create', 'RestCategoryController@createCategory')
		 ->name('rest-category-create')
		 ->middleware('user');

	//修改使用者的分類
	Route::post('/category/update', 'RestCategoryController@updateCategory')
		 ->name('rest-category-update')
		 ->middleware('user')
		 ->middleware('check-project-member')
		 ->middleware('check-category-owner');

	//刪除使用者的分類
	Route::post('/category/delete', 'RestCategoryController@deleteCategory')
		 ->name('rest-category-delete')
		 ->middleware('user')
		 ->middleware('check-category-owner');

	//取得使用者的所有專案
	Route::get('/project/get', 'RestProjectController@getProject')
		 ->name('rest-project-get')
		 ->middleware('user');

	//取得使用者的單一專案
	Route::get('/project/fetch/{project_id}', 'RestProjectController@fetchProject')
		 ->name('rest-project-fetch')
		 ->middleware('user');

	//建立使用者的專案
	Route::post('/project/create', 'RestProjectController@createProject')
		 ->name('rest-project-create')
		 ->middleware('user');

	//修改使用者的專案
	Route::post('/project/update', 'RestProjectController@updateProject')
		 ->name('rest-project-update')
		 ->middleware('user')
		 ->middleware('check-project-member')
		 ->middleware('check-project-role');

	//刪除使用者的專案
	Route::post('/project/delete', 'RestProjectController@deleteProject')
		 ->name('rest-project-delete')
		 ->middleware('user')
		 ->middleware('check-project-member')
		 ->middleware('check-project-role');

	//邀請成員加入專案
	Route::post('/invite/send', 'RestInviteController@sendInvitation')
		 ->name('rest-invite-send')
		 ->middleware('user')
		 ->middleware('check-project-member')
		 ->middleware('check-project-role');

	//讓成員退出專案
	Route::post('/member/delete', 'RestMemberController@deleteMember')
		 ->name('rest-delete-member')
		 ->middleware('user')
		 ->middleware('check-project-member')
		 ->middleware('check-project-role');

	//取得使用者的事項
	Route::get('/item/get/{status?}', 'RestItemController@getItem')
		 ->name('rest-item-get')
		 ->middleware('user');

	//取得使單筆事項
	Route::get('/item/fetch/{id}', 'RestItemController@fetchItem')
		 ->name('rest-item-fetch')
		 ->middleware('user');

	//建立使用者的事項
	Route::post('/item/create', 'RestItemController@createItem')
		 ->name('rest-item-create')
		 ->middleware('user')
		 ->middleware('item');

	//更新使用者的事項
	Route::post('/item/update', 'RestItemController@updateItem')
		 ->name('rest-item-update')
		 ->middleware('user')
		 ->middleware('item');

	//開始處理事項
	Route::post('/item/process', 'RestItemController@processItem')
		 ->name('rest-item-process')
		 ->middleware('user');

	//完成事項
	Route::post('/item/complete', 'RestItemController@completeItem')
		 ->name('rest-item-complete')
		 ->middleware('user');

	//更新事項的順序
	Route::post('/item/refresh/order', 'RestItemController@refreshItemOrder')
		 ->name('rest-item-refresh-order')
		 ->middleware('user');

	//刪除使用者的事項
	Route::post('/item/delete', 'RestItemController@deleteItem')
		 ->name('rest-item-delete')
		 ->middleware('user');

	//更新CSRF Token
	Route::get('/csrf/refresh', function() {
		$ret = [];
		$ret['csrf'] = csrf_token();
		return response()->json($ret);
	})->middleware('user');
});