<?php

use Illuminate\Http\Request;

Route::prefix('service')->group(function() {
	Route::get('/term', 'ServiceController@term')->name('service-term');
});