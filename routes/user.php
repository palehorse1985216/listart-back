<?php

use Illuminate\Http\Request;

Route::prefix('user')->group(function() {
	//會員基本資料設定
	Route::get('/setting', 'UserController@setting')->name('user-setting')
													->middleware('user');
	//會員修改密碼
	Route::get('/password', 'UserController@password')->name('user-password')
													->middleware('user');
});
