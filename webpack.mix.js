const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('resources/sass/app.scss', 'public/css')
   .sass('resources/sass/login-show.scss', 'public/css')
   .sass('resources/sass/login-password.scss', 'public/css')
   .sass('resources/sass/authenticate.scss', 'public/css')
   .sass('resources/sass/item.scss', 'public/css')
   .sass('resources/sass/user-setting.scss', 'public/css')
   .sass('resources/sass/user-password.scss', 'public/css')
   .sass('resources/sass/project.scss', 'public/css')
   .sass('resources/sass/project-manage.scss', 'public/css')
   .sass('resources/sass/failure.scss', 'public/css')
   .sass('resources/sass/privacy-policy.scss', 'public/css')
   .sass('resources/sass/service-term.scss', 'public/css')
   .js(['resources/js/app.js', 'resources/js/header.js'], 'public/js/header.js')
   .js(['resources/js/app.js', 'resources/js/login-show.js'], 'public/js/login-show.js')
   .js(['resources/js/app.js', 'resources/js/login-password.js'], 'public/js/login-password.js')
   .js(['resources/js/app.js', 'resources/js/item.js'], 'public/js/item.js')
   .js(['resources/js/app.js', 'resources/js/user-setting.js'], 'public/js/user-setting.js')
   .js(['resources/js/app.js', 'resources/js/user-password.js'], 'public/js/user-password.js')
   .js(['resources/js/app.js', 'resources/js/project.js'], 'public/js/project.js')
   .js(['resources/js/app.js', 'resources/js/project-manage.js'], 'public/js/project-manage.js')
   .js(['resources/js/app.js'], 'public/js/privacy-policy.js')
   .js(['resources/js/app.js'], 'public/js/service-term.js')